/**
    bambam
    Copyright (C) 2019 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
#if ! defined(BIOBAMBAM2_READHER_HPP)
#define BIOBAMBAM2_READHER_HPP

#include <utility>
#include <string>

namespace biobambam2
{
	struct HeaderUpdate
	{
		virtual ~HeaderUpdate() {}
		virtual std::string operator()(std::string const & text) const = 0;
	};


	struct ReHeaderUpdate : public biobambam2::HeaderUpdate
	{
		std::string const text;
		bool const append;

		ReHeaderUpdate(std::string const & rtext, bool const rappend)
		: text(rtext), append(rappend)
		{

		}

		std::string operator()(std::string const & header) const
		{
			if ( append )
				return header + text;
			else
				return text;
		}
	};
}

std::pair<uint64_t,std::string> readHeader(std::istream & in, biobambam2::HeaderUpdate const & HU, int const level, bool const dropheader = false);
#endif
