/**
    biobambam
    Copyright (C) 2019 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
#if ! defined(BIOBAMBAM2_DEPTHINTERVAL_HPP)
#define BIOBAMBAM2_DEPTHINTERVAL_HPP

#include <libmaus2/util/NumberSerialisation.hpp>
#include <libmaus2/math/IntegerInterval.hpp>

namespace biobambam2
{
	struct DepthInterval
	{
		uint64_t refid;
		uint64_t from;
		uint64_t to;
		uint64_t depth;

		DepthInterval() {}
		DepthInterval(
			uint64_t const rrefid,
			uint64_t const rfrom,
			uint64_t const rto,
			uint64_t const rdepth
		) : refid(rrefid), from(rfrom), to(rto), depth(rdepth) {}

		libmaus2::math::IntegerInterval<int64_t> getInterval() const
		{
			return libmaus2::math::IntegerInterval<int64_t>(from,static_cast<int64_t>(to)-1);
		}

		std::ostream & serialise(std::ostream & out) const
		{
			libmaus2::util::NumberSerialisation::serialiseNumber(out,refid);
			libmaus2::util::NumberSerialisation::serialiseNumber(out,from);
			libmaus2::util::NumberSerialisation::serialiseNumber(out,to);
			libmaus2::util::NumberSerialisation::serialiseNumber(out,depth);
			return out;
		}

		std::istream & deserialise(std::istream & in)
		{
			refid = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
			from = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
			to = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
			depth = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
			return in;
		}

		bool operator<(DepthInterval const & O) const
		{
			if ( refid != O.refid )
				return refid < O.refid;
			else if ( from != O.from )
				return from < O.from;
			else
				return to > O.to;
		}
	};
}

std::ostream & operator<<(std::ostream & out, biobambam2::DepthInterval const & D);
#endif
