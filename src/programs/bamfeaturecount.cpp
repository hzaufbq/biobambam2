/**
    bambam
    Copyright (C) 2019 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
#include <config.h>

#include <libmaus2/util/ArgInfo.hpp>
#include <libmaus2/lz/PlainOrGzipStream.hpp>
#include <libmaus2/util/LineBuffer.hpp>
#include <libmaus2/util/TabEntry.hpp>
#include <libmaus2/trie/TrieState.hpp>
#include <libmaus2/geometry/RangeSet.hpp>
#include <libmaus2/bambam/BamMultiAlignmentDecoderFactory.hpp>
#include <libmaus2/bambam/BamNumericalIndexDecoder.hpp>
#include <libmaus2/bambam/BamNumericalIndexGenerator.hpp>
#include <libmaus2/sorting/SortingBufferedOutputFile.hpp>
#include <libmaus2/aio/GenericPeeker.hpp>

#include <libmaus2/parallel/NumCpus.hpp>

#include <biobambam2/Licensing.hpp>
#include <biobambam2/Reference.hpp>

static int getDefaultVerbose() { return 0; }
static int getDefaultExportCDNA() { return 0; }
static int getDefaultMapQMin() { return 255; }
static int getDefaultMapQMax() { return 255; }
static double getDefaultUncoveredThres() { return 0.1; }
static double getDefaultUniqueUncoveredThres() { return 0.1; }
static uint64_t getDefaultNumThreads() { return libmaus2::parallel::NumCpus::getNumLogicalProcessors(); }

static std::string formatDouble(double const v)
{
	std::ostringstream ostr;
	ostr << v;
	return ostr.str();
}

static void clipQuotes(std::pair<char const *,char const *> & P)
{
	if (
		P.second - P.first >= 2
		&&
		P.first[0] == '"'
		&&
		P.second[-1] == '"'
	)
	{
		++P.first;
		--P.second;
	}
}

static uint64_t parseNumber(std::pair<char const *,char const *> P)
{
	uint64_t n = 0, nc = 0;
	char const * a = P.first;
	char const * e = P.second;

	while ( a != e && a[0] >= '0' && a[0] <= '9' )
	{
		n *= 10;
		n += (*(a++)) - '0';
		nc += 1;
	}

  	if ( nc && a == e )
		return n;
	else
	{
		libmaus2::exception::LibMausException lme;
		lme.getStream() << "[E] unable to parse number " << std::string(P.first,P.second) << std::endl;
		lme.finish();
		throw lme;
	}
}

static bool parseStrand(std::pair<char const *,char const *> P)
{
	if ( P.second-P.first == 1 && P.first[0] == '+' )
		return true;
	else if ( P.second-P.first == 1 && P.first[0] == '-' )
		return false;
	else
	{
		libmaus2::exception::LibMausException lme;
		lme.getStream() << "[E] unable to parse strand " << std::string(P.first,P.second) << std::endl;
		lme.finish();
		throw lme;
	}
}

static uint64_t copyString(libmaus2::autoarray::AutoArray<char> & A, std::pair<char const *,char const *> const & P)
{
	std::ptrdiff_t const l = P.second-P.first;
	A.ensureSize(l);
	std::copy(P.first,P.second,A.begin());
	return l;
}

static uint64_t pushId(
	libmaus2::autoarray::AutoArray<char> & Aid,
	uint64_t & o_in,
	libmaus2::autoarray::AutoArray<char> const & A,
	uint64_t const A_o
)
{
	uint64_t const o_r = o_in;

	Aid.ensureSize(o_in+A_o+1);
	std::copy(A.begin(),A.begin()+A_o,Aid.begin() + o_in);
	Aid[o_in + A_o] = 0;
	o_in += (A_o + 1);
	return o_r;
}

struct CDS
{
	uint64_t id;
	uint64_t start;
	uint64_t end;
	bool strand;
	uint64_t exonid;

	CDS() {}
	CDS(
		uint64_t const rid,
		uint64_t const rstart,
		uint64_t const rend,
		bool const rstrand,
		uint64_t const rexonid
	) : id(rid), start(rstart), end(rend), strand(rstrand), exonid(rexonid)
	{
	}

	std::ostream & serialise(std::ostream & out) const
	{
		libmaus2::util::NumberSerialisation::serialiseNumber(out,id);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,start);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,end);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,strand);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,exonid);
		return out;
	}

	std::istream & deserialise(std::istream & in)
	{
		id = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		start = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		end = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		strand = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		exonid = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		return in;
	}

	int64_t getFrom() const
	{
		return static_cast<int64_t>(start)-1;
	}

	int64_t getTo() const
	{
		return end;
	}
};

std::ostream & operator<<(std::ostream & out, CDS const & C)
{
	out << "CDS(id=" << C.id << ",start=" << C.start << ",end=" << C.end << ",strand=" << C.strand << ",exonid=" << C.exonid << ")";
	return out;
}

struct ExonSubInfo
{
	uint64_t uncovered;
	uint64_t basecount;
	uint64_t total;

	ExonSubInfo()
	: uncovered(0), basecount(0), total(0)
	{
	}

	ExonSubInfo & operator+=(ExonSubInfo const & E)
	{
		uncovered += E.uncovered;
		basecount += E.basecount;
		total += E.total;
		return *this;
	}

	ExonSubInfo operator+(ExonSubInfo const & E) const
	{
		ExonSubInfo T = *this;
		T += E;
		return T;
	}
	double getUncoveredFraction() const
	{
		if ( total )
		{
			return uncovered / static_cast<double>(total);
		}
		else
		{
			return 0;
		}
	}
};

std::ostream & operator<<(std::ostream & out, ExonSubInfo const & E)
{
	double const dtotal = E.total;
	if ( E.total )
		out << "(" << E.uncovered/dtotal << "," << E.basecount/dtotal << "," << E.total << ")";
	else
		out << "*";

	return out;
}

struct ExonInfo
{
	ExonSubInfo unique;
	ExonSubInfo ambig;

	ExonInfo() : unique(), ambig() {}

	ExonInfo & operator+=(ExonInfo const & E)
	{
		unique += E.unique;
		ambig += E.ambig;
		return *this;
	}

	double getUncoveredFraction() const
	{
		uint64_t const u = unique.uncovered + ambig.uncovered;
		uint64_t const t = unique.total + ambig.total;
		assert ( t );
		double const dt = t;
		return u / dt;
	}
};

std::ostream & operator<<(std::ostream & out, ExonInfo const & E)
{
	out << (E.unique + E.ambig);
	return out;
}

struct Exon
{
	uint64_t id;
	uint64_t id_offset;
	uint64_t start;
	uint64_t end;
	bool strand;
	uint64_t transcript_id;
	uint64_t CDS_start;
	uint64_t CDS_end;

	bool operator==(Exon const & E) const
	{
		return
			id == E.id
			&&
			id_offset == E.id_offset
			&&
			start == E.start
			&&
			end == E.end
			&&
			strand == E.strand
			&&
			transcript_id == E.transcript_id
			&&
			CDS_start == E.CDS_start
			&&
			CDS_end == E.CDS_end
			;
	}

	std::ostream & serialise(std::ostream & out) const
	{
		libmaus2::util::NumberSerialisation::serialiseNumber(out,id);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,id_offset);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,start);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,end);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,strand);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,transcript_id);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,CDS_start);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,CDS_end);
		return out;
	}

	std::istream & deserialise(std::istream & in)
	{
		id = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		id_offset = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		start = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		end = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		strand = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		transcript_id = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		CDS_start = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		CDS_end = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		return in;
	}

	Exon()
	{}

	Exon(
		uint64_t const rstart,
		uint64_t const rend
	) : start(rstart+1), end(rend) {}

	Exon(
		uint64_t const rid,
		uint64_t const rid_offset,
		uint64_t const rstart,
		uint64_t const rend,
		bool const rstrand,
		uint64_t const rtranscript_id,
		uint64_t const rCDS_start,
		uint64_t const rCDS_end
	) : id(rid), id_offset(rid_offset), start(rstart), end(rend), strand(rstrand), transcript_id(rtranscript_id), CDS_start(rCDS_start), CDS_end(rCDS_end)
	{}

	libmaus2::math::IntegerInterval<int64_t> getInterval() const
	{
		return libmaus2::math::IntegerInterval<int64_t>(getFrom(),getTo()-1);
	}

	std::pair<uint64_t,uint64_t> getPair() const
	{
		libmaus2::math::IntegerInterval<int64_t> const IA = getInterval();
		return std::pair<uint64_t,uint64_t>(IA.from,IA.to+1);
	}

	int64_t getFrom() const
	{
		return static_cast<int64_t>(start)-1;
	}

	int64_t getTo() const
	{
		return end;
	}

	bool operator<(Exon const & E) const
	{
		if ( start != E.start )
			return start < E.start;
		else
			return end > E.end;
	}

	std::ostream & printCoord(std::ostream & out) const
	{
		 out << "[" << getFrom() << "," << getTo() << ")";
		 return out;
	}
};

std::ostream & operator<<(std::ostream & out, Exon const & E)
{
	out << "Exon(" <<
		"id=" << E.id << ","
		"id_offset=" << E.id_offset << ","
		"start=" << E.start << ","
		"end=" << E.end << ","
		"strand=" << E.strand << ","
		"transcript_id=" << E.transcript_id << ","
		"CDS_start=" << E.CDS_start << ","
		"CDS_end=" << E.CDS_end << ")";
	return out;
}

struct ExonIdOffsetComparator
{
	bool operator()(Exon const & A, Exon const & B) const
	{
		return A.id_offset < B.id_offset;
	}
};

struct Transcript
{
	uint64_t id_offset;
	uint64_t start;
	uint64_t end;
	bool strand;
	uint64_t gene_id;

	Transcript() {}
	Transcript(
		uint64_t const rid_offset,
		uint64_t const rstart,
		uint64_t const rend,
		bool const rstrand,
		uint64_t const rgene_id
	) : id_offset(rid_offset), start(rstart), end(rend), strand(rstrand), gene_id(rgene_id)
	{

	}

	std::ostream & serialise(std::ostream & out) const
	{
		libmaus2::util::NumberSerialisation::serialiseNumber(out,id_offset);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,start);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,end);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,strand);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,gene_id);
		return out;
	}

	std::istream & deserialise(std::istream & in)
	{
		id_offset = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		start = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		end = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		strand = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		gene_id = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		return in;
	}
};

struct Gene
{
	uint64_t chr_offset;
	uint64_t chr_id;
	uint64_t id_offset;
	uint64_t name_offset;
	uint64_t start;
	uint64_t end;
	uint64_t cstart;
	uint64_t cend;

	std::ostream & serialise(std::ostream & out) const
	{
		libmaus2::util::NumberSerialisation::serialiseNumber(out,chr_offset);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,chr_id);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,id_offset);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,name_offset);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,start);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,end);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,cstart);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,cend);
		return out;
	}

	std::istream & deserialise(std::istream & in)
	{
		chr_offset = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		chr_id = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		id_offset = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		name_offset = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		start = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		end = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		cstart = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		cend = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		return in;
	}

	Gene() {}
	Gene(
		uint64_t const rchr_offset,
		uint64_t const rid_offset,
		uint64_t const rname_offset,
		uint64_t const rstart,
		uint64_t const rend,
		uint64_t const rcstart,
		uint64_t const rcend
	) : chr_offset(rchr_offset), chr_id(std::numeric_limits<uint64_t>::max()), id_offset(rid_offset), name_offset(rname_offset), start(rstart), end(rend), cstart(rcstart), cend(rcend)
	{

	}
};

#if 0
static std::string print(Gene const & G, libmaus2::autoarray::AutoArray<char> const & Aid)
{
	std::ostringstream ostr;

	ostr << "Gene("
		<< Aid.begin() + G.chr_offset << ","
		<< G.chr_id << ","
		<< Aid.begin() + G.id_offset << ","
		<< Aid.begin() + G.name_offset << ","
		<< G.start << ","
		<< G.end << ","
		<< G.cstart << ","
		<< G.cend << ","
		<< G.freq << ")";

	return ostr.str();
}
#endif

struct GeneChrComparator
{
	char const * id;

	GeneChrComparator(char const * rid)
	: id(rid)
	{
	}

	char const * get(Gene const & G) const
	{
		return id + G.chr_offset;
	}

	bool operator()(Gene const & A, Gene const & B) const
	{
		return strcmp(get(A),get(B)) < 0;
	}

};

struct ExonInterval
{
	uint64_t chr;
	uint64_t from;
	uint64_t to;

	ExonInterval()
	{
	}

	ExonInterval(uint64_t const rchr, uint64_t const rfrom, uint64_t const rto)
	: chr(rchr), from(rfrom), to(rto)
	{
	}

	bool operator<(ExonInterval const & RHS) const
	{
		if ( chr != RHS.chr )
			return chr < RHS.chr;
		else if ( from != RHS.from )
			return from < RHS.from;
		else
			return to > RHS.to;
	}

	std::ostream & serialise(std::ostream & out) const
	{
		libmaus2::util::NumberSerialisation::serialiseNumber(out,chr);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,from);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,to);
		return out;
	}

	std::istream & deserialise(std::istream & in)
	{
		chr = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		from = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		to = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		return in;
	}
};

std::ostream & operator<<(std::ostream & out, ExonInterval const & U)
{
	out << "ExonInterval([" << U.from << "," << U.to << "))";
	return out;
}

struct IntervalList
{
	typedef IntervalList this_type;
	typedef libmaus2::util::unique_ptr<this_type>::type unique_ptr_type;

	struct IntervalWithId
	{
		std::pair<uint64_t,uint64_t> Q;
		uint64_t start;
		uint64_t end;

		IntervalWithId(
			std::pair<uint64_t,uint64_t> const rQ,
			uint64_t const rstart,
			uint64_t const rend
		) : Q(rQ), start(rstart), end(rend) {}

		IntervalWithId() : Q(), start(0), end(0) {}
		IntervalWithId(uint64_t const Qf) : Q(Qf,0), start(0), end(0) {}

		std::string toString() const
		{
			std::ostringstream ostr;
			ostr << "IntervalWithId([" << Q.first << "," << Q.second << ")," << start << "," << end << ")";
			return ostr.str();
		}
	};


	struct IntervalWithIdPosComparator
	{
		bool operator()(IntervalWithId const & A, IntervalWithId const & B) const
		{
			return A.Q.first < B.Q.first;
		}
	};

	uint64_t getId(ExonInterval const & E) const
	{
		IntervalWithId const * it = std::lower_bound(
			V.begin(),
			V.begin()+V_o,
			IntervalWithId(E.from),
			IntervalWithIdPosComparator()
		);

		if ( it != V.begin() + V_o && it->Q.first == E.from )
			return it - V.begin();
		else
		{
			assert ( it != V.begin() );
			it -= 1;
			assert ( E.from > it->Q.first );
			assert ( E.from < it->Q.second );

			return it-V.begin();
		}
	}

	IntervalWithId const & get(uint64_t const id) const
	{
		return V.at(id);
	}

	libmaus2::autoarray::AutoArray<uint64_t> Aid;
	uint64_t Aid_o;

	libmaus2::autoarray::AutoArray<IntervalWithId> V;
	uint64_t V_o;

	// intervals unique to a single exon
	libmaus2::autoarray::AutoArray<ExonInterval> AU;
	uint64_t AU_o;

	libmaus2::autoarray::AutoArray< std::atomic<uint64_t> >::unique_ptr_type AC;

	libmaus2::util::FiniteSizeHeap< std::pair<uint64_t,uint64_t> > FSHcopy;

	IntervalList()
	: Aid_o(0), V_o(0), AU_o(0), FSHcopy(0)
	{

	}

	std::ostream & printUnique(std::ostream & out) const
	{
		for ( uint64_t i = 0; i < AU_o; ++i )
			out << AU[i] << "\n";
		return out;
	}

	static bool intersect(std::pair<uint64_t,uint64_t> const & P, std::pair<uint64_t,uint64_t> const & Q)
	{
		if ( Q.first < P.first )
			return intersect(Q,P);

		assert ( P.first <= Q.first );

		return P.second > Q.first;
	}

	struct QueryContext
	{
		libmaus2::autoarray::AutoArray<uint64_t> A;
		libmaus2::autoarray::AutoArray<Exon> E;
		libmaus2::autoarray::AutoArray<ExonInterval> EI;
	};

	std::pair<uint64_t,uint64_t> query(std::pair<uint64_t,uint64_t> const & Q, QueryContext & context, Exon const * E, uint64_t const chr) const
	{
		if ( ! V_o )
			return std::pair<uint64_t,uint64_t>(0,0);

		// search for intervals
		IntervalWithId const * lp =
			std::lower_bound(
				V.begin(),
				V.begin()+V_o,
				IntervalWithId(Q.first),
				IntervalWithIdPosComparator()
			);

		// adjust
		while ( lp != V.begin() && intersect(lp[-1].Q,Q) )
			--lp;

		assert (
			lp == V.begin() || !intersect(lp[-1].Q,Q)
		);

		uint64_t o = 0;
		uint64_t EI_o = 0;

		libmaus2::math::IntegerInterval<int64_t> const IQ(Q.first,static_cast<int64_t>(Q.second)-1);

		// while interval overlap
		for ( ; (lp < V.begin() + V_o) && intersect(lp->Q,Q); ++lp )
		{
			// interval covered by any exon
			if ( lp->end != lp->start )
			{
				// how many exons cover this interval?
				// uint64_t const freq = lp->end - lp->start;

				for ( uint64_t i = lp->start; i < lp->end; ++i )
					context.A.push(o,Aid[i]);

				libmaus2::math::IntegerInterval<int64_t> const IL(lp->Q.first,static_cast<int64_t>(lp->Q.second)-1);
				libmaus2::math::IntegerInterval<int64_t> const IC = IQ.intersection(IL);
				assert ( ! IC.isEmpty() );
				ExonInterval const UI(chr,IC.from,IC.from + IC.diameter());
				context.EI.push(EI_o,UI);

				// std::cerr << "found " << lp->Q.first << "," << lp->Q.second << " for " << Q.first << "," << Q.second << std::endl;
			}
		}

		assert ( lp == V.begin() + V_o || lp->Q.first >= Q.second );

		std::sort(context.A.begin(),context.A.begin()+o);
		o = std::unique(context.A.begin(),context.A.begin()+o) - context.A.begin();

		uint64_t oe = 0;
		for ( uint64_t i = 0; i < o; ++i )
		{
			Exon const & exon = E[context.A[i]];
			context.E.push(oe,exon);
			// std::cerr << "found " << exon.getFrom() << "," << exon.getTo() << " for " << Q.first << "," << Q.second << std::endl;
		}

		return std::pair<uint64_t,uint64_t>(oe,EI_o);
	}

	void validate(std::pair<uint64_t,uint64_t> const & Q, uint64_t const id) const
	{
		IntervalWithId const * lp =
			std::lower_bound(
				V.begin(),
				V.begin()+V_o,
				IntervalWithId(Q.first),
				IntervalWithIdPosComparator()
			);

		IntervalWithId const * hp = lp;
		while ( hp != V.begin() + V_o && hp->Q.second < Q.second )
			++hp;

		assert ( lp != V.begin() + V_o );
		assert ( hp != V.begin() + V_o );
		assert ( lp->Q.first == Q.first );
		assert ( hp->Q.second == Q.second );

		hp += 1;

		for ( IntervalWithId const * ip = lp; ip < hp; ++ip )
		{
			bool found = false;
			for ( uint64_t i = ip->start; (!found) && (i < ip->end); ++i )
				if ( Aid[i] == id )
					found = true;

			if ( ! found )
			{
				std::cerr << "validate " << Q.first << "," << Q.second << " id=" << id << std::endl;

				std::cerr << "lp->Q.first=" << lp->Q.first << std::endl;
				std::cerr << "hp[-1].Q.second=" << hp[-1].Q.second << std::endl;

				std::cerr << "ip->Q=[" << ip->Q.first << "," << ip->Q.second << ")" << std::endl;
				for ( uint64_t i = ip->start; (i < ip->end); ++i )
					std::cerr << "\tid\t" << Aid[i] << std::endl;
			}

			assert ( found );
		}
	}

	void push(std::pair<uint64_t,uint64_t> Q, libmaus2::util::FiniteSizeHeap< std::pair<uint64_t,uint64_t> > const & FSH, uint64_t const chr)
	{
		assert ( Q.second > Q.first );

		IntervalWithId I;
		I.Q = Q;
		I.start = Aid_o;

		FSHcopy.copyFrom(FSH);
		while ( !FSHcopy.empty() )
			Aid.push(Aid_o,FSHcopy.pop().second);

		I.end = Aid_o;

		// if interval is only covered by a single exon
		if ( I.end - I.start == 1 )
			AU.push(AU_o,ExonInterval(chr,Q.first,Q.second));

		assert ( V_o == 0 || I.Q.first >= V[V_o-1].Q.second );

		V.push(V_o,I);
	}

	static unique_ptr_type construct(Exon const * LAP64, uint64_t const LOP64, uint64_t const chr)
	{
		libmaus2::util::FiniteSizeHeap< std::pair<uint64_t,uint64_t> > FSH(0);
		uint64_t prevstart = 0;

		IntervalList::unique_ptr_type pIL(new IntervalList);

		for ( uint64_t j = 0; j < LOP64; ++j )
		{
			Exon const & exon = LAP64[j];
			std::pair<uint64_t,uint64_t> const P = exon.getPair();

			while ( !FSH.empty() && FSH.top().first <= P.first )
			{
				// push interval and active exon ids
				if ( FSH.top().first > prevstart )
				{
					std::pair<uint64_t,uint64_t> Q(prevstart,FSH.top().first);
					pIL->push(Q,FSH,chr);
				}

				prevstart = FSH.top().first;
				FSH.pop();
			}

			// push interval and active exon ids
			if ( P.first > prevstart )
			{
				std::pair<uint64_t,uint64_t> Q(prevstart,P.first);
				pIL->push(Q,FSH,chr);
			}

			prevstart = P.first;
			FSH.pushBump(std::pair<uint64_t,uint64_t>(P.second,j));
		}

		while ( !FSH.empty() )
		{
			// push interval and active exon ids
			if ( FSH.top().first > prevstart )
			{
				std::pair<uint64_t,uint64_t> Q(prevstart,FSH.top().first);
				pIL->push(Q,FSH,chr);
			}

			prevstart = FSH.top().first;
			FSH.pop();
		}

		for ( uint64_t j = 0; j < LOP64; ++j )
			pIL->validate(LAP64[j].getPair(),j);

		libmaus2::autoarray::AutoArray< std::atomic<uint64_t> >::unique_ptr_type TAC(
			new libmaus2::autoarray::AutoArray< std::atomic<uint64_t> >(pIL->V_o)
		);
		pIL->AC = UNIQUE_PTR_MOVE(TAC);

		return pIL;
	}
};


struct GTFData
{
	typedef GTFData this_type;
	typedef libmaus2::util::unique_ptr<this_type>::type unique_ptr_type;

	libmaus2::autoarray::AutoArray<char> Aid;

	libmaus2::autoarray::AutoArray<CDS> ACDS;
	libmaus2::autoarray::AutoArray<Exon> Aexon;
	libmaus2::autoarray::AutoArray<Transcript> Atranscript;
	libmaus2::autoarray::AutoArray<Gene> Agene;

	std::vector<std::string> Vchr;

	uint64_t Aid_o;
	uint64_t next_gene_id;
	uint64_t next_transcript_id;
	uint64_t next_exon_id;
	uint64_t next_cds_id;

	static std::string getCacheFileName(std::string const & annofn)
	{
		return annofn + ".feature_cache";
	}

	static bool validCacheExists(std::string const & annofn)
	{
		std::string const cachefn = getCacheFileName(annofn);

		if (
			(! libmaus2::util::GetFileSize::fileExists(cachefn))
			||
			libmaus2::util::GetFileSize::isOlder(cachefn,annofn)
		)
			return false;
		else
			return true;
	}

	void createCacheIf(std::string const & annofn, int const verbose) const
	{
		if ( (! validCacheExists(annofn)) || (! readEOF(getCacheFileName(annofn))) )
		{
			std::string const cachefn = getCacheFileName(annofn);

			if ( verbose )
				std::cerr << "[V] cache fn " << cachefn << std::endl;

			libmaus2::aio::OutputStreamInstance OSI(cachefn);
			serialise(OSI);
			OSI.flush();
		}
	}

	static std::string getEOF()
	{
		std::ostringstream ostr;
		for ( uint64_t i = 0; i < 8; ++i )
			ostr.put(i);
		ostr << "EOF";
		for ( uint64_t i = 0; i < 8; ++i )
			ostr.put(static_cast<uint8_t>(i^0xff));

		return ostr.str();
	}

	static bool readEOF(std::istream & in)
	{
		std::string const data = getEOF();
		libmaus2::autoarray::AutoArray<char> A(data.size());
		in.read(A.begin(),data.size());

		if ( in.gcount() != static_cast<int64_t>(data.size()) )
			return false;

		std::string const cdata(A.begin(),A.end());

		return data == cdata;
	}

	static bool readEOF(std::string const & cachefn)
	{
		libmaus2::aio::InputStreamInstance ISI(cachefn);
		std::string const data = getEOF();

		ISI.seekg(-static_cast<int64_t>(data.size()),std::ios::end);

		return readEOF(ISI);
	}

	std::ostream & serialise(std::ostream & out) const
	{
		libmaus2::util::NumberSerialisation::serialiseNumber(out,Aid_o);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,next_gene_id);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,next_transcript_id);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,next_exon_id);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,next_cds_id);

		out.write(Aid.begin(),Aid_o);
		for ( uint64_t i = 0; i < next_cds_id; ++i )
			ACDS[i].serialise(out);
		for ( uint64_t i = 0; i < next_exon_id; ++i )
			Aexon[i].serialise(out);
		for ( uint64_t i = 0; i < next_transcript_id; ++i )
			Atranscript[i].serialise(out);
		for ( uint64_t i = 0; i < next_gene_id; ++i )
			Agene[i].serialise(out);

		libmaus2::util::StringSerialisation::serialiseStringVector(out,Vchr);

		std::string const eof = getEOF();
		out.write(eof.c_str(),eof.size());

		if ( ! out )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] failed to serialise GTFData object" << std::endl;
			lme.finish();
			throw lme;
		}

		return out;
	}

	std::istream & deserialise(std::istream & in)
	{
		Aid_o = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		next_gene_id = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		next_transcript_id = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		next_exon_id = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		next_cds_id = libmaus2::util::NumberSerialisation::deserialiseNumber(in);

		Aid.ensureSize(Aid_o);
		in.read(Aid.begin(),Aid_o);

		ACDS.ensureSize(next_cds_id);
		for ( uint64_t i = 0; i < next_cds_id; ++i )
			ACDS[i].deserialise(in);
		Aexon.ensureSize(next_exon_id);
		for ( uint64_t i = 0; i < next_exon_id; ++i )
			Aexon[i].deserialise(in);
		Atranscript.ensureSize(next_transcript_id);
		for ( uint64_t i = 0; i < next_transcript_id; ++i )
			Atranscript[i].deserialise(in);
		Agene.ensureSize(next_gene_id);
		for ( uint64_t i = 0; i < next_gene_id; ++i )
			Agene[i].deserialise(in);

		Vchr = libmaus2::util::StringSerialisation::deserialiseStringVector(in);

		bool const eofok = readEOF(in);

		if ( !eofok )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] failed to read eof block in GTFData::deserialise" << std::endl;
			lme.finish();
			throw lme;
		}

		return in;
	}

	void reset()
	{
		Aid_o = 0;
		next_gene_id = 0;
		next_transcript_id = 0;
		next_exon_id = 0;
		next_cds_id = 0;
	}

	GTFData()
	{
		reset();
	}

	GTFData(std::string const & annofn, int const verbose)
	{
		reset();

		libmaus2::aio::InputStreamInstance ISI(annofn);
		libmaus2::lz::PlainOrGzipStream POG(ISI);
		libmaus2::util::LineBuffer LB(POG);
		char const * a;
		char const * e;
		libmaus2::util::TabEntry<> TE;
		libmaus2::util::TabEntry<';'> TE8;
		libmaus2::util::TabEntry<' '> TES;

		static char const * c_gene = "gene";
		std::ptrdiff_t const l_gene = strlen(c_gene);
		static char const * c_transcript = "transcript";
		std::ptrdiff_t const l_transcript = strlen(c_transcript);
		static char const * c_exon = "exon";
		std::ptrdiff_t const l_exon = strlen(c_exon);
		static char const * c_CDS = "CDS";
		std::ptrdiff_t const l_CDS = strlen(c_CDS);

		static char const * c_gene_id = "gene_id";
		std::ptrdiff_t const l_gene_id = strlen(c_gene_id);
		static char const * c_transcript_id = "transcript_id";
		std::ptrdiff_t const l_transcript_id = strlen(c_transcript_id);
		static char const * c_gene_name = "gene_name";
		std::ptrdiff_t const l_gene_name = strlen(c_gene_name);

		static char const * c_exon_id = "exon_id";
		std::ptrdiff_t const l_exon_id = strlen(c_exon_id);
		static char const * c_exon_number = "exon_number";
		std::ptrdiff_t const l_exon_number = strlen(c_exon_number);

		libmaus2::autoarray::AutoArray<char> A_gene_id;
		std::ptrdiff_t A_gene_id_o = 0;
		libmaus2::autoarray::AutoArray<char> A_gene_name;
		std::ptrdiff_t A_gene_name_o = 0;
		libmaus2::autoarray::AutoArray<char> A_gene_chr;
		std::ptrdiff_t A_gene_chr_o = 0;

		libmaus2::autoarray::AutoArray<char> A_transcript_id;
		std::ptrdiff_t A_transcript_id_o = 0;
		libmaus2::autoarray::AutoArray<char> A_transcript_chr;
		std::ptrdiff_t A_transcript_chr_o = 0;

		libmaus2::autoarray::AutoArray<char> A_exon_id;
		std::ptrdiff_t A_exon_id_o = 0;
		libmaus2::autoarray::AutoArray<char> A_exon_chr;
		std::ptrdiff_t A_exon_chr_o = 0;
		libmaus2::autoarray::AutoArray<char> A_exon_number;
		std::ptrdiff_t A_exon_number_o = 0;

		bool have_gene = false;
		bool have_transcript = false;
		bool have_exon = false;
		// bool have_CDS = false;
		uint64_t exonidnext = std::numeric_limits<uint64_t>::max();
		uint64_t lineid = 0;

		for ( ; LB.getline(&a,&e); ++lineid )
		{
			if ( a && e-a && a[0] != '#' )
			{
				TE.parse(a,a,e);

				if ( TE.size() >= 8 )
				{
					std::pair<char const *,char const *> const P2 = TE.get(2,a);

					if ( P2.second-P2.first == l_gene && strncmp(P2.first,c_gene,l_gene) == 0 )
					{
						std::pair<char const *,char const *> const P8 = TE.get(8,a);
						TE8.parse(P8.first,P8.first,P8.second);

						bool havegeneid = false;
						bool havegenename = false;

						for ( uint64_t i = 0; i < TE8.size(); ++i )
						{
							std::pair<char const *,char const *> P8i = TE8.get(i,P8.first);
							if ( P8i.first != P8i.second && ::isspace(*P8i.first) )
								++P8i.first;

							TES.parse(P8i.first,P8i.first,P8i.second);

							if ( TES.size() == 2 )
							{
								std::pair<char const *,char const *> P80 = TES.get(0,P8i.first);
								std::pair<char const *,char const *> P81 = TES.get(1,P8i.first);

								if ( P80.second-P80.first == l_gene_id && strncmp(P80.first,c_gene_id,l_gene_id) == 0 )
								{
									clipQuotes(P81);
									A_gene_id_o = copyString(A_gene_id,P81);
									havegeneid = true;
								}
								else if ( P80.second-P80.first == l_gene_name && strncmp(P80.first,c_gene_name,l_gene_name) == 0 )
								{
									clipQuotes(P81);
									A_gene_name_o = copyString(A_gene_name,P81);
									havegenename = true;
								}
							}
						}

						if ( havegeneid && havegenename )
						{
							A_gene_chr_o = copyString(A_gene_chr,TE.get(0,a));

							#if 0
							std::cerr << "gene"
								<< "\t" << std::string(A_gene_chr.begin(),A_gene_chr.begin()+A_gene_chr_o)
								<< "\t" << std::string(A_gene_id.begin(),A_gene_id.begin()+A_gene_id_o)
								<< "\t" << std::string(A_gene_name.begin(),A_gene_name.begin()+A_gene_name_o)
								<< "\n"
								;
							#endif

							uint64_t const off_gene_id = pushId(Aid,Aid_o,A_gene_id,A_gene_id_o);
							uint64_t const off_chr_id = pushId(Aid,Aid_o,A_gene_chr,A_gene_chr_o);
							uint64_t const off_gene_name = pushId(Aid,Aid_o,A_gene_name,A_gene_name_o);

							uint64_t const cstart = parseNumber(TE.get(3,a));
							uint64_t const cend = parseNumber(TE.get(4,a));
							// bool const strand = parseStrand(TE.get(6,a));

							Agene.push(
								next_gene_id,
								Gene(
									off_chr_id,
									off_gene_id,
									off_gene_name,
									next_transcript_id,
									next_transcript_id,
									cstart,
									cend
								)
							);

							have_gene = true;
						}
						else
						{
							libmaus2::exception::LibMausException lme;
							lme.getStream() << "[E] broken gene line: " << std::string(a,e) << std::endl;
							lme.finish();
							throw lme;
						}
					}
					else if ( P2.second-P2.first == l_transcript && strncmp(P2.first,c_transcript,l_transcript) == 0 )
					{
						if ( ! have_gene )
						{
							libmaus2::exception::LibMausException lme;
							lme.getStream() << "[E] transcript line not matching gene meta data " << std::string(a,e) << std::endl;
							lme.finish();
							throw lme;
						}
						// std::cerr << "transcript " << std::string(a,e) << std::endl;

						bool havegeneid = false;
						bool havegenename = false;
						bool havetranscriptid = false;

						std::pair<char const *,char const *> const P8 = TE.get(8,a);
						TE8.parse(P8.first,P8.first,P8.second);

						for ( uint64_t i = 0; i < TE8.size(); ++i )
						{
							std::pair<char const *,char const *> P8i = TE8.get(i,P8.first);
							if ( P8i.first != P8i.second && ::isspace(*P8i.first) )
								++P8i.first;

							TES.parse(P8i.first,P8i.first,P8i.second);

							if ( TES.size() == 2 )
							{
								std::pair<char const *,char const *> P80 = TES.get(0,P8i.first);
								std::pair<char const *,char const *> P81 = TES.get(1,P8i.first);

								if ( P80.second-P80.first == l_gene_id && strncmp(P80.first,c_gene_id,l_gene_id) == 0 )
								{
									clipQuotes(P81);

									if (
										P81.second-P81.first == A_gene_id_o
										&&
										strncmp(A_gene_id.begin(),P81.first,P81.second-P81.first) == 0
									)
									{
										havegeneid = true;
									}
									else
									{
										libmaus2::exception::LibMausException lme;
										lme.getStream() << "[E] transcript not matching gene meta data id " << std::string(a,e) << std::endl;
										lme.finish();
										throw lme;
									}
								}
								else if ( P80.second-P80.first == l_gene_name && strncmp(P80.first,c_gene_name,l_gene_name) == 0 )
								{
									clipQuotes(P81);

									if (
										P81.second-P81.first == A_gene_name_o
										&&
										strncmp(A_gene_name.begin(),P81.first,P81.second-P81.first) == 0
									)
									{
										havegenename = true;
									}
									else
									{
										libmaus2::exception::LibMausException lme;
										lme.getStream() << "[E] transcript not matching gene meta data name " << std::string(a,e) << std::endl;
										lme.finish();
										throw lme;
									}
								}
								else if ( P80.second-P80.first == l_transcript_id && strncmp(P80.first,c_transcript_id,l_transcript_id) == 0 )
								{
									clipQuotes(P81);
									A_transcript_id_o = copyString(A_transcript_id,P81);
									havetranscriptid = true;
								}
							}
						}

						if ( havegeneid && havegenename && havetranscriptid )
						{
							A_transcript_chr_o = copyString(A_transcript_chr,TE.get(0,a));

							if (
								A_transcript_chr_o != A_gene_chr_o
								||
								strncmp(A_transcript_chr.begin(),A_gene_chr.begin(),A_transcript_chr_o) != 0
							)
							{
								libmaus2::exception::LibMausException lme;
								lme.getStream() << "[E] broken transcript line (refseq mismatch with gene): " << std::string(a,e) << std::endl;
								lme.finish();
								throw lme;
							}

							#if 0
							std::cerr << "transcript"
								<< "\t" << std::string(A_transcript_chr.begin(),A_transcript_chr.begin()+A_transcript_chr_o)
								<< "\t" << std::string(A_gene_id.begin(),A_gene_id.begin()+A_gene_id_o)
								<< "\t" << std::string(A_gene_name.begin(),A_gene_name.begin()+A_gene_name_o)
								<< "\t" << std::string(A_transcript_id.begin(),A_transcript_id.begin()+A_transcript_id_o)
								<< "\n"
								;
							#endif

							bool const strand = parseStrand(TE.get(6,a));

							uint64_t const off_transcript_id = pushId(Aid,Aid_o,A_transcript_id,A_transcript_id_o);

							Atranscript.push(
								next_transcript_id,
								Transcript(
									off_transcript_id,
									next_exon_id,
									next_exon_id,
									strand,
									next_gene_id-1
								)
							);
							Agene[next_gene_id-1].end += 1;


							have_transcript = true;
							exonidnext = 1;
						}
						else
						{
							libmaus2::exception::LibMausException lme;
							lme.getStream() << "[E] broken transcript line: " << std::string(a,e) << std::endl;
							lme.finish();
							throw lme;
						}
					}
					else if ( P2.second-P2.first == l_exon && strncmp(P2.first,c_exon,l_exon) == 0 )
					{
						if ( ! have_transcript )
						{
							libmaus2::exception::LibMausException lme;
							lme.getStream() << "[E] exon line not matching transcript meta data " << std::string(a,e) << std::endl;
							lme.finish();
							throw lme;
						}
						// std::cerr << "transcript " << std::string(a,e) << std::endl;

						bool havegeneid = false;
						bool havegenename = false;
						bool havetranscriptid = false;
						bool haveexonid = false;
						bool haveexonnumber = false;

						std::pair<char const *,char const *> const P8 = TE.get(8,a);
						TE8.parse(P8.first,P8.first,P8.second);

						for ( uint64_t i = 0; i < TE8.size(); ++i )
						{
							std::pair<char const *,char const *> P8i = TE8.get(i,P8.first);
							if ( P8i.first != P8i.second && ::isspace(*P8i.first) )
								++P8i.first;

							TES.parse(P8i.first,P8i.first,P8i.second);

							if ( TES.size() == 2 )
							{
								std::pair<char const *,char const *> P80 = TES.get(0,P8i.first);
								std::pair<char const *,char const *> P81 = TES.get(1,P8i.first);

								if ( P80.second-P80.first == l_gene_id && strncmp(P80.first,c_gene_id,l_gene_id) == 0 )
								{
									clipQuotes(P81);

									if (
										P81.second-P81.first == A_gene_id_o
										&&
										strncmp(A_gene_id.begin(),P81.first,P81.second-P81.first) == 0
									)
									{
										havegeneid = true;
									}
									else
									{
										libmaus2::exception::LibMausException lme;
										lme.getStream() << "[E] exon not matching transcript meta data id " << std::string(a,e) << std::endl;
										lme.finish();
										throw lme;
									}
								}
								else if ( P80.second-P80.first == l_gene_name && strncmp(P80.first,c_gene_name,l_gene_name) == 0 )
								{
									clipQuotes(P81);

									if (
										P81.second-P81.first == A_gene_name_o
										&&
										strncmp(A_gene_name.begin(),P81.first,P81.second-P81.first) == 0
									)
									{
										havegenename = true;
									}
									else
									{
										libmaus2::exception::LibMausException lme;
										lme.getStream() << "[E] exon not matching transcript meta data name " << std::string(a,e) << std::endl;
										lme.finish();
										throw lme;
									}
								}
								else if ( P80.second-P80.first == l_transcript_id && strncmp(P80.first,c_transcript_id,l_transcript_id) == 0 )
								{
									clipQuotes(P81);

									if (
										P81.second-P81.first == A_transcript_id_o
										&&
										strncmp(A_transcript_id.begin(),P81.first,P81.second-P81.first) == 0
									)
									{
										havetranscriptid = true;
									}
									else
									{
										libmaus2::exception::LibMausException lme;
										lme.getStream() << "[E] exon not matching transcript meta data name " << std::string(a,e) << std::endl;
										lme.finish();
										throw lme;
									}
								}
								else if ( P80.second-P80.first == l_exon_id && strncmp(P80.first,c_exon_id,l_exon_id) == 0 )
								{
									clipQuotes(P81);
									A_exon_id_o = copyString(A_exon_id,P81);
									haveexonid = true;
								}
								else if ( P80.second-P80.first == l_exon_number && strncmp(P80.first,c_exon_number,l_exon_number) == 0 )
								{
									clipQuotes(P81);
									A_exon_number_o = copyString(A_exon_number,P81);
									haveexonnumber = true;
								}
							}
						}

						if ( havegeneid && havegenename && havetranscriptid && haveexonid && haveexonnumber )
						{
							A_exon_chr_o = copyString(A_exon_chr,TE.get(0,a));

							uint64_t uexonnumber = 0;
							uint64_t uexonnumber_c = 0;
							char const * cexonnumbera = A_exon_number.begin();
							char const * cexonnumbere = A_exon_number.begin() + A_exon_number_o;
							while ( cexonnumbera != cexonnumbere && *cexonnumbera >= '0' && *cexonnumbera <= '9' )
							{
								uexonnumber *= 10;
								uexonnumber += (*(cexonnumbera++)) - '0';
								uexonnumber_c += 1;
							}

							if ( cexonnumbera != cexonnumbere || !uexonnumber_c )
							{
								libmaus2::exception::LibMausException lme;
								lme.getStream() << "[E] broken exon line (unparsable exon_number): " << std::string(a,e) << std::endl;
								lme.finish();
								throw lme;
							}
							if ( uexonnumber != exonidnext )
							{
								libmaus2::exception::LibMausException lme;
								lme.getStream() << "[E] broken exon line (exon number out of sequence) " << std::string(a,e) << std::endl;
								lme.finish();
								throw lme;
							}

							uint64_t const start = parseNumber(TE.get(3,a));
							uint64_t const end = parseNumber(TE.get(4,a));
							bool const strand = parseStrand(TE.get(6,a));

							if (
								A_exon_chr_o != A_gene_chr_o
								||
								strncmp(A_exon_chr.begin(),A_gene_chr.begin(),A_exon_chr_o) != 0
							)
							{
								libmaus2::exception::LibMausException lme;
								lme.getStream() << "[E] broken exon line (refseq mismatch with transcript): " << std::string(a,e) << std::endl;
								lme.finish();
								throw lme;
							}

							#if 0
							std::cerr << "exon"
								<< "\t" << std::string(A_exon_chr.begin(),A_exon_chr.begin()+A_exon_chr_o)
								<< "\t" << std::string(A_exon_id.begin(),A_exon_id.begin()+A_exon_id_o)
								<< "\t" << uexonnumber
								<< "\t" << start
								<< "\t" << end
								<< "\t" << std::string(A_gene_id.begin(),A_gene_id.begin()+A_gene_id_o)
								<< "\t" << std::string(A_gene_name.begin(),A_gene_name.begin()+A_gene_name_o)
								<< "\t" << std::string(A_transcript_id.begin(),A_transcript_id.begin()+A_transcript_id_o)
								<< "\n"
								;
							#endif

							uint64_t const off_exon_id = pushId(Aid,Aid_o,A_exon_id,A_exon_id_o);
							uint64_t const this_exon_id = next_exon_id;

							have_exon = true;

							Aexon.push(next_exon_id,Exon(this_exon_id,off_exon_id,start,end,strand,next_transcript_id-1,next_cds_id,next_cds_id));
							Atranscript[next_transcript_id-1].end += 1;

							exonidnext += 1;
						}
						else
						{
							libmaus2::exception::LibMausException lme;
							lme.getStream() << "[E] broken exon line: " << std::string(a,e) << std::endl;
							lme.finish();
							throw lme;
						}
					}
					else if ( P2.second-P2.first == l_CDS && strncmp(P2.first,c_CDS,l_CDS) == 0 )
					{
						if ( ! have_exon )
						{
							libmaus2::exception::LibMausException lme;
							lme.getStream() << "[E] CDS line not matching exon meta data " << std::string(a,e) << std::endl;
							lme.finish();
							throw lme;
						}
						// std::cerr << "transcript " << std::string(a,e) << std::endl;

						bool havegeneid = false;
						bool havegenename = false;
						bool havetranscriptid = false;
						bool haveexonid = false;
						bool haveexonnumber = false;

						std::pair<char const *,char const *> const P8 = TE.get(8,a);
						TE8.parse(P8.first,P8.first,P8.second);

						for ( uint64_t i = 0; i < TE8.size(); ++i )
						{
							std::pair<char const *,char const *> P8i = TE8.get(i,P8.first);
							if ( P8i.first != P8i.second && ::isspace(*P8i.first) )
								++P8i.first;

							TES.parse(P8i.first,P8i.first,P8i.second);

							if ( TES.size() == 2 )
							{
								std::pair<char const *,char const *> P80 = TES.get(0,P8i.first);
								std::pair<char const *,char const *> P81 = TES.get(1,P8i.first);

								if ( P80.second-P80.first == l_gene_id && strncmp(P80.first,c_gene_id,l_gene_id) == 0 )
								{
									clipQuotes(P81);

									if (
										P81.second-P81.first == A_gene_id_o
										&&
										strncmp(A_gene_id.begin(),P81.first,P81.second-P81.first) == 0
									)
									{
										havegeneid = true;
									}
									else
									{
										libmaus2::exception::LibMausException lme;
										lme.getStream() << "[E] CDS not matching exon meta data id " << std::string(a,e) << std::endl;
										lme.finish();
										throw lme;
									}
								}
								else if ( P80.second-P80.first == l_gene_name && strncmp(P80.first,c_gene_name,l_gene_name) == 0 )
								{
									clipQuotes(P81);

									if (
										P81.second-P81.first == A_gene_name_o
										&&
										strncmp(A_gene_name.begin(),P81.first,P81.second-P81.first) == 0
									)
									{
										havegenename = true;
									}
									else
									{
										libmaus2::exception::LibMausException lme;
										lme.getStream() << "[E] CDS not matching exon meta data name " << std::string(a,e) << std::endl;
										lme.finish();
										throw lme;
									}
								}
								else if ( P80.second-P80.first == l_transcript_id && strncmp(P80.first,c_transcript_id,l_transcript_id) == 0 )
								{
									clipQuotes(P81);

									if (
										P81.second-P81.first == A_transcript_id_o
										&&
										strncmp(A_transcript_id.begin(),P81.first,P81.second-P81.first) == 0
									)
									{
										havetranscriptid = true;
									}
									else
									{
										libmaus2::exception::LibMausException lme;
										lme.getStream() << "[E] CDS not matching exon meta data name " << std::string(a,e) << std::endl;
										lme.finish();
										throw lme;
									}
								}
								else if ( P80.second-P80.first == l_exon_id && strncmp(P80.first,c_exon_id,l_exon_id) == 0 )
								{
									clipQuotes(P81);

									if (
										P81.second-P81.first == A_exon_id_o
										&&
										strncmp(A_exon_id.begin(),P81.first,P81.second-P81.first) == 0
									)
									{
										haveexonid = true;
									}
									else
									{
										libmaus2::exception::LibMausException lme;
										lme.getStream() << "[E] CDS not matching exon meta data name " << std::string(a,e) << std::endl;
										lme.finish();
										throw lme;
									}
								}
								else if ( P80.second-P80.first == l_exon_number && strncmp(P80.first,c_exon_number,l_exon_number) == 0 )
								{
									clipQuotes(P81);

									if (
										P81.second-P81.first == A_exon_number_o
										&&
										strncmp(A_exon_number.begin(),P81.first,P81.second-P81.first) == 0
									)
									{

										haveexonnumber = true;
									}
									else
									{
										libmaus2::exception::LibMausException lme;
										lme.getStream() << "[E] CDS not matching exon meta data name " << std::string(a,e) << std::endl;
										lme.finish();
										throw lme;
									}
								}
							}
						}

						if ( havegeneid && havegenename && havetranscriptid && haveexonid && haveexonnumber )
						{
							uint64_t const start = parseNumber(TE.get(3,a));
							uint64_t const end = parseNumber(TE.get(4,a));
							bool const strand = parseStrand(TE.get(6,a));

							uint64_t const this_CDS_id = next_cds_id;

							// have_CDS = true;

							ACDS.push(next_cds_id,CDS(this_CDS_id,start,end,strand,next_exon_id-1));
							Aexon[next_exon_id-1].CDS_end += 1;

							std::cerr << "found " << ACDS[next_cds_id-1] << " for " << Aexon[next_exon_id-1] << std::endl;
						}
						else
						{
							libmaus2::exception::LibMausException lme;
							lme.getStream() << "[E] broken exon line: " << std::string(a,e) << std::endl;
							lme.finish();
							throw lme;
						}
					}
				}
			}

			if ( verbose && (lineid % (1024*1024) == 0) && (lineid != 0) )
			{
				std::cerr << "[V] processed " << lineid << " annotation lines" << std::endl;
			}
		}

		if ( verbose )
		{
			std::cerr << "[V] processed " << lineid << " annotation lines" << std::endl;
			std::cerr << "[V] number of genes " << next_gene_id << std::endl;
			std::cerr << "[V] number of transcripts " << next_transcript_id << std::endl;
			std::cerr << "[V] number of exons " << next_exon_id << std::endl;
		}

		libmaus2::autoarray::AutoArray<Gene> Agenecopy(next_gene_id);
		std::copy(Agene.begin(),Agene.begin()+next_gene_id,Agenecopy.begin());
		GeneChrComparator const GIC(Aid.begin());
		std::sort(Agenecopy.begin(),Agenecopy.begin()+next_gene_id,GIC);

		uint64_t low = 0;
		while ( low < next_gene_id )
		{
			uint64_t high = low+1;

			while ( high < next_gene_id && ! GIC(Agenecopy[low],Agenecopy[high]) )
				++high;

			#if 0
			std::cerr << GIC.get(Agenecopy[low]) << "\t" << high-low << std::endl;
			#endif

			Vchr.push_back(GIC.get(Agenecopy[low]));

			low = high;
		}

		// std::sort(Vchr.begin(),Vchr.end());
		for ( uint64_t i = 1; i < Vchr.size(); ++i )
			assert ( Vchr[i-1] < Vchr[i] );

		::libmaus2::trie::Trie<char> trienofailure;
		trienofailure.insertContainer(Vchr);
		::libmaus2::trie::LinearHashTrie<char,uint32_t>::unique_ptr_type LHTnofailure(trienofailure.toLinearHashTrie<uint32_t>());

		for ( uint64_t i = 0; i < next_gene_id; ++i )
		{
			char const * chr = Aid.begin() + Agene[i].chr_offset;
			int64_t const id = LHTnofailure->searchCompleteNoFailureZ(chr);
			assert ( id >= 0 );
			Agene[i].chr_id = id;
		}
	}
};

static libmaus2::bambam::BamHeader::unique_ptr_type readBamHeader(libmaus2::util::ArgInfo const & arginfo, std::string const & bamfn)
{
	libmaus2::util::ArgInfo arginfoCopy = arginfo;
	arginfoCopy.replaceKey("I",bamfn);

	libmaus2::bambam::BamAlignmentDecoderWrapper::unique_ptr_type bamdecwrapper(
		libmaus2::bambam::BamMultiAlignmentDecoderFactory::construct(arginfoCopy)
	);
	libmaus2::bambam::BamHeader::unique_ptr_type ubamheader(bamdecwrapper->getDecoder().getHeader().uclone());

	return ubamheader;
}

std::string printHelpMessage(libmaus2::util::ArgInfo const & arginfo)
{
	std::ostringstream ostr;

	ostr << ::biobambam2::Licensing::license();

	ostr << std::endl;

	ostr << "synopsis: " << arginfo.progname << " [threads=1] <annotation.gtf> <in.bam>" << std::endl;

	ostr << std::endl;
	ostr << "Key=Value pairs:" << std::endl;
	ostr << std::endl;

	std::vector< std::pair<std::string,std::string> > V;

	V.push_back ( std::pair<std::string,std::string> ( "verbose=<["+::biobambam2::Licensing::formatNumber(getDefaultVerbose())+"]>", "print progress report (default: 1)" ) );
	V.push_back ( std::pair<std::string,std::string> ( "mapqmin=<["+::biobambam2::Licensing::formatNumber(getDefaultMapQMin())+"]>", "minimum mapping quality processed (default: 255)" ) );
	V.push_back ( std::pair<std::string,std::string> ( "mapqmax=<["+::biobambam2::Licensing::formatNumber(getDefaultMapQMax())+"]>", "maximum mapping quality processed (default: 255)" ) );
	V.push_back ( std::pair<std::string,std::string> ( "threads=<["+::biobambam2::Licensing::formatNumber(1)+"]>", "number of threads used (default: 1, use 0 for number cores on machine)" ) );
	V.push_back ( std::pair<std::string,std::string> ( "uncoveredthres=<["+::formatDouble(getDefaultUncoveredThres())+"]>", "threshold for fraction of uncovered exon bases per transcript (default: 0.1)" ) );
	V.push_back ( std::pair<std::string,std::string> ( "uniqueuncoveredthres=<["+::formatDouble(getDefaultUniqueUncoveredThres())+"]>", "threshold for fraction of uncovered unique exon bases per transcript (default: 0.1)" ) );
	V.push_back ( std::pair<std::string,std::string> ( "T=<filename>", "prefix for temporary files, default: create files in current directory" ) );
	V.push_back ( std::pair<std::string,std::string> ( "exclude=<[SECONDARY]>", "exclude alignments matching any of the given flags" ) );
	V.push_back ( std::pair<std::string,std::string> ( "exportcdna=<["+::biobambam2::Licensing::formatNumber(getDefaultExportCDNA())+"]>", "export CDNA FastA file using annotation and reference, then quit (requires reference parameter to be set, default: 0)" ) );
	V.push_back ( std::pair<std::string,std::string> ( "reference=<[]>", "name of reference FastA file" ) );

	::biobambam2::Licensing::printMap(ostr,V);

	ostr << std::endl;
	ostr << "Alignment flags: PAIRED,PROPER_PAIR,UNMAP,MUNMAP,REVERSE,MREVERSE,READ1,READ2,SECONDARY,QCFAIL,DUP,SUPPLEMENTARY" << std::endl;

	return ostr.str();
}


int bamfeaturecount(libmaus2::util::ArgInfo const & arginfo)
{
	bool const strandspecific = false;
	unsigned int numthreads = arginfo.getValue<unsigned int>("threads",1);
	unsigned int verbose = arginfo.getValue<unsigned int>("verbose",getDefaultVerbose());
	unsigned int exportcdna = arginfo.getValue<unsigned int>("exportcdna",getDefaultExportCDNA());
	if ( numthreads == 0 )
		numthreads = getDefaultNumThreads();
	uint32_t const excludeflags = libmaus2::bambam::BamFlagBase::stringToFlags(arginfo.getUnparsedValue("exclude","SECONDARY"));
	std::string const tmpfilenamebase = arginfo.getValue<std::string>("T",arginfo.getDefaultTmpFileName());
	int const mapqmin = arginfo.getValue<int>("mapqmin",getDefaultMapQMin());
	int const mapqmax = arginfo.getValue<int>("mapqmax",getDefaultMapQMax());
	double const uncoveredthres = arginfo.getValue<double>("uncoveredthres",getDefaultUncoveredThres());
	double const uniqueuncoveredthres = arginfo.getValue<double>("uniqueuncoveredthres",getDefaultUniqueUncoveredThres());

	std::string const annofn = arginfo.getUnparsedRestArg(0);
	std::string const bamfn = arginfo.getUnparsedRestArg(1);


	GTFData::unique_ptr_type pGTF;

	if ( GTFData::validCacheExists(annofn) )
	{
		GTFData::unique_ptr_type tGTF(new GTFData);
		libmaus2::aio::InputStreamInstance ISI(GTFData::getCacheFileName(annofn));
		tGTF->deserialise(ISI);
		pGTF = UNIQUE_PTR_MOVE(tGTF);
		if ( verbose )
			std::cerr << "[V] obtained GTF data from cache" << std::endl;
	}
	else
	{
		GTFData::unique_ptr_type tGTF(new GTFData(annofn,verbose));
		pGTF = UNIQUE_PTR_MOVE(tGTF);
		pGTF->createCacheIf(annofn,verbose);
	}

	GTFData const & gtfdata = *pGTF;

	if ( exportcdna )
	{
		if ( !arginfo.hasArg("reference") )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] required argument reference is missing" << std::endl;
			lme.finish();
			throw lme;
		}

		std::string const fafn = arginfo.getUnparsedValue("reference",std::string());

		Reference ref(fafn);

		for ( uint64_t ti = 0; ti < gtfdata.next_transcript_id; ++ti )
		{
			Transcript const & T = gtfdata.Atranscript[ti];
			Gene const & G = gtfdata.Agene[T.gene_id];
			char const * chr = gtfdata.Aid.begin() + G.chr_offset;
			uint64_t const refid = ref.getIdFor(chr);
			std::string const & s = ref.Vtext.at(refid);

			bool const strand = (T.end-T.start) ? gtfdata.Aexon[T.start].strand : true;

			std::ostringstream ostr;

			for ( uint64_t ei = 0; ei < T.end-T.start; ++ei )
			{
				uint64_t const rei =
					strand ? ei : ((T.end-T.start)-ei-1);

				Exon const & E = gtfdata.Aexon[T.start + rei];

				uint64_t const from = E.getFrom();
				uint64_t const to = E.getTo();

				ostr << s.substr(from,to-from);
			}

			std::string cdna = ostr.str();
			for ( uint64_t i = 0; i < cdna.size(); ++i )
				cdna[i] = libmaus2::fastx::remapChar(libmaus2::fastx::mapChar(cdna[i]));

			if ( !strand )
				cdna = libmaus2::fastx::reverseComplementUnmapped(cdna);

			char const * ctrans = gtfdata.Aid.begin() + T.id_offset;
			std::cout << ">" << ctrans << "\n" << cdna << "\n";
		}

		return EXIT_SUCCESS;
	}

	libmaus2::bambam::BamHeader::unique_ptr_type ubamheader(readBamHeader(arginfo,bamfn));
	libmaus2::bambam::BamHeader const & bamheader = *ubamheader;

	std::string const indexfn = libmaus2::bambam::BamNumericalIndexBase::getIndexName(bamfn);
	libmaus2::bambam::BamNumericalIndexGenerator::indexFileCheck(bamfn,indexfn,1024/*block size*/,numthreads/*threads*/);
	libmaus2::bambam::BamNumericalIndexDecoder indexdec(indexfn);
	uint64_t const numaln = indexdec.getAlignmentCount();

	uint64_t const packsperthread = 32;
	uint64_t const targetpacks = numthreads * packsperthread;

	uint64_t const alperthread = (numaln + targetpacks - 1)/targetpacks;
	uint64_t const alpacks = alperthread ? (numaln + alperthread - 1)/alperthread : 0;

	std::vector<int64_t> Vmap(bamheader.getNumRef(),-1);
	std::vector<int64_t> Vrmap(gtfdata.Vchr.size(),-1);

	for ( uint64_t i = 0; i < bamheader.getNumRef(); ++i )
	{
		std::string const srefname = bamheader.getRefIDName(i);
		typedef std::vector<std::string>::const_iterator it;
		std::pair<it,it> const P = std::equal_range(gtfdata.Vchr.begin(),gtfdata.Vchr.end(),srefname);
		if ( P.second != P.first )
		{
			assert ( P.second - P.first == 1 );
			uint64_t const chrid = P.first - gtfdata.Vchr.begin();
			// std::cerr << srefname << "\t" << chrid << std::endl;

			Vmap.at(i) = chrid;
			Vrmap.at(chrid) = i;
		}
		#if 0
		else
		{
			std::cerr << "[V] " << srefname << " not found in annotation file" << std::endl;
		}
		#endif
	}

	std::vector<uint64_t> Vmax(gtfdata.Vchr.size(),0);

	for ( uint64_t i = 0; i < gtfdata.next_exon_id; ++i )
	{
		Exon const & exon = gtfdata.Aexon[i];
		Transcript const & transcript = gtfdata.Atranscript[exon.transcript_id];
		Gene const & gene = gtfdata.Agene[transcript.gene_id];
		uint64_t const high = exon.getTo();
		uint64_t const chrid = gene.chr_id;
		Vmax[chrid] = std::max(Vmax[chrid],high);
	}

	libmaus2::autoarray::AutoArray< libmaus2::geometry::RangeSet<Exon>::unique_ptr_type > VRS_forward(gtfdata.Vchr.size());
	libmaus2::autoarray::AutoArray< libmaus2::geometry::RangeSet<Exon>::unique_ptr_type > VRS_reverse(gtfdata.Vchr.size());
	libmaus2::autoarray::AutoArray< libmaus2::geometry::RangeSet<Exon>::unique_ptr_type > VRS(gtfdata.Vchr.size());

	for ( uint64_t i = 0; i < gtfdata.Vchr.size(); ++i )
	{
		if ( Vrmap[i] >= 0 )
		{
			assert ( bamheader.getRefIDLength(Vrmap[i]) >= static_cast<int64_t>(Vmax[i]) );
			Vmax[i] = bamheader.getRefIDLength(Vrmap[i]);
		}

		{
			libmaus2::geometry::RangeSet<Exon>::unique_ptr_type RS(new libmaus2::geometry::RangeSet<Exon>(Vmax[i]));
			VRS_forward[i] = UNIQUE_PTR_MOVE(RS);
		}
		{
			libmaus2::geometry::RangeSet<Exon>::unique_ptr_type RS(new libmaus2::geometry::RangeSet<Exon>(Vmax[i]));
			VRS_reverse[i] = UNIQUE_PTR_MOVE(RS);
		}
		{
			libmaus2::geometry::RangeSet<Exon>::unique_ptr_type RS(new libmaus2::geometry::RangeSet<Exon>(Vmax[i]));
			VRS[i] = UNIQUE_PTR_MOVE(RS);
		}
	}

	for ( uint64_t i = 0; i < gtfdata.next_exon_id; ++i )
	{
		Exon const & exon = gtfdata.Aexon[i];
		Transcript const & transcript = gtfdata.Atranscript[exon.transcript_id];
		Gene const & gene = gtfdata.Agene[transcript.gene_id];
		uint64_t const chrid = gene.chr_id;

		if ( exon.strand )
			VRS_forward[chrid]->insert(exon);
		else
			VRS_reverse[chrid]->insert(exon);

		VRS[chrid]->insert(exon);
	}

	#define RANGE_QUERY_CHECK

	#if defined(RANGE_QUERY_CHECK)
	// exon array
	typedef libmaus2::autoarray::AutoArray<Exon> ap64;
	// array of exon arrays
	libmaus2::autoarray::AutoArray<ap64::unique_ptr_type> AP64(gtfdata.Vchr.size());
	libmaus2::autoarray::AutoArray<ap64::unique_ptr_type> AP64_forward(gtfdata.Vchr.size());
	libmaus2::autoarray::AutoArray<ap64::unique_ptr_type> AP64_reverse(gtfdata.Vchr.size());
	// size of exon arrays
	libmaus2::autoarray::AutoArray<uint64_t> OP64(gtfdata.Vchr.size());
	libmaus2::autoarray::AutoArray<uint64_t> OP64_forward(gtfdata.Vchr.size());
	libmaus2::autoarray::AutoArray<uint64_t> OP64_reverse(gtfdata.Vchr.size());
	// array of interval lists
	libmaus2::autoarray::AutoArray<IntervalList::unique_ptr_type> AIL(gtfdata.Vchr.size());
	libmaus2::autoarray::AutoArray<IntervalList::unique_ptr_type> AIL_forward(gtfdata.Vchr.size());
	libmaus2::autoarray::AutoArray<IntervalList::unique_ptr_type> AIL_reverse(gtfdata.Vchr.size());
	for ( uint64_t i = 0; i < gtfdata.Vchr.size(); ++i )
	{
		{
			ap64::unique_ptr_type tptr(new ap64);
			AP64[i] = UNIQUE_PTR_MOVE(tptr);
			OP64[i] = 0;
		}
		{
			ap64::unique_ptr_type tptr(new ap64);
			AP64_forward[i] = UNIQUE_PTR_MOVE(tptr);
			OP64_forward[i] = 0;
		}
		{
			ap64::unique_ptr_type tptr(new ap64);
			AP64_reverse[i] = UNIQUE_PTR_MOVE(tptr);
			OP64_reverse[i] = 0;
		}
	}
	// bucket sort exons into arrays
	for ( uint64_t i = 0; i < gtfdata.next_exon_id; ++i )
	{
		Exon const & exon = gtfdata.Aexon[i];
		Transcript const & transcript = gtfdata.Atranscript[exon.transcript_id];
		Gene const & gene = gtfdata.Agene[transcript.gene_id];
		uint64_t const chrid = gene.chr_id;

		AP64[chrid]->push(OP64[chrid],exon);
		if ( exon.strand )
			AP64_forward[chrid]->push(OP64_forward[chrid],exon);
		else
			AP64_reverse[chrid]->push(OP64_reverse[chrid],exon);
	}
	// initialize interval lists
	for ( uint64_t i = 0; i < gtfdata.Vchr.size(); ++i )
	{
		std::sort(AP64[i]->begin(),AP64[i]->begin() + OP64[i]);
		IntervalList::unique_ptr_type pIL(IntervalList::construct(AP64[i]->begin(),OP64[i],i));
		AIL[i] = UNIQUE_PTR_MOVE(pIL);

		// AIL[i]->printUnique(std::cerr);
	}
	libmaus2::sorting::SerialisingSortingBufferedOutputFileArray<ExonInterval>::unique_ptr_type sortPtr;
	{
		std::string const sorttmp = tmpfilenamebase + "_sorttmp";
		typedef libmaus2::sorting::SerialisingSortingBufferedOutputFileArray<ExonInterval> sorter_type;
		sorter_type::unique_ptr_type tsortPtr(
			new sorter_type(
				sorttmp,numthreads,sorter_type::getDefaultBufSize(),true /* register files as temp */
			)
		);
		sortPtr = UNIQUE_PTR_MOVE(tsortPtr);
	}
	#endif

	for ( uint64_t i = 0; i < gtfdata.next_gene_id; ++i )
	{
		Gene const & gene = gtfdata.Agene[i];
		char const * id = gtfdata.Aid.begin() + gene.chr_offset;
		assert ( gtfdata.Vchr[gene.chr_id] == id );
	}

	struct ThreadContext
	{
		typedef ThreadContext this_type;
		typedef libmaus2::util::unique_ptr<this_type>::type unique_ptr_type;

		libmaus2::autoarray::AutoArray < Exon const * > query_R1;
		libmaus2::autoarray::AutoArray < Exon const * > query_R2;
		libmaus2::util::SimpleQueue<libmaus2::geometry::RangeSet<Exon>::search_q_element> query_todo;
		libmaus2::autoarray::AutoArray<uint64_t> AGID1;
		libmaus2::autoarray::AutoArray<uint64_t> AGID2;
		libmaus2::autoarray::AutoArray<uint64_t> ATID1;
		libmaus2::autoarray::AutoArray<uint64_t> ATID2;
		libmaus2::autoarray::AutoArray<libmaus2::bambam::cigar_operation> Aop;
		#if defined(RANGE_QUERY_CHECK)
		IntervalList::QueryContext ILQC_1;
		IntervalList::QueryContext ILQC_2;
		#endif
	};

	libmaus2::autoarray::AutoArray<ThreadContext::unique_ptr_type> AT(numthreads);
	for ( uint64_t t = 0; t < numthreads; ++t )
	{
		ThreadContext::unique_ptr_type tptr(new ThreadContext);
		AT[t] = UNIQUE_PTR_MOVE(tptr);
	}

	#if 0
	for ( uint64_t i = 0; i < bamheader.getNumRef(); ++i )
		std::cerr << "bam id " << i << " " << bamheader.getRefIDName(i) << " -> " << Vmap[i] << std::endl;
	#endif

	std::atomic<uint64_t> algnid(0);
	std::atomic<uint64_t> numunique(0);
	std::atomic<uint64_t> numunmatched(0);
	std::atomic<uint64_t> numambig(0);
	std::atomic<uint64_t> numsplit(0);
	std::vector< std::atomic<uint64_t> > Vnumunmatched(gtfdata.Vchr.size());
	std::vector< std::atomic<uint64_t> > Vnumunique(gtfdata.Vchr.size());
	std::vector< std::atomic<uint64_t> > Vnumambig(gtfdata.Vchr.size());

	std::vector< std::atomic<uint64_t> > exonFreq(gtfdata.Aexon.size());
	std::vector< std::atomic<uint64_t> > geneFreq(gtfdata.Agene.size());
	std::vector< std::atomic<uint64_t> > geneBases(gtfdata.Agene.size());
	std::atomic<uint64_t> packComplete(0);


	#if defined(_OPENMP)
	#pragma omp parallel for num_threads(numthreads) schedule(dynamic,1)
	#endif
	for ( uint64_t t = 0; t < alpacks; ++t )
	{
		#if defined(_OPENMP)
		uint64_t const tid = omp_get_thread_num();
		#else
		uint64_t const tid = 0;
		#endif

		uint64_t const tlow = t * alperthread;
		uint64_t const thigh = std::min(tlow + alperthread,numaln);
		uint64_t const tsize = thigh-tlow;

		libmaus2::lz::BgzfInflateFile::unique_ptr_type pdecoder(indexdec.getStreamAt(bamfn,tlow));
		libmaus2::bambam::BamAlignment algn;

		ThreadContext & TC = *(AT[tid]);

		libmaus2::autoarray::AutoArray < Exon const * > & query_R1 = TC.query_R1;
		libmaus2::autoarray::AutoArray < Exon const * > & query_R2 = TC.query_R2;
		libmaus2::util::SimpleQueue<libmaus2::geometry::RangeSet<Exon>::search_q_element> & query_todo = TC.query_todo;
		libmaus2::autoarray::AutoArray<uint64_t> & AGID1 = TC.AGID1;
		libmaus2::autoarray::AutoArray<uint64_t> & AGID2 = TC.AGID2;
		libmaus2::autoarray::AutoArray<uint64_t> & ATID1 = TC.ATID1;
		libmaus2::autoarray::AutoArray<uint64_t> & ATID2 = TC.ATID2;
		libmaus2::autoarray::AutoArray<libmaus2::bambam::cigar_operation> & Aop = TC.Aop;

		#if defined(RANGE_QUERY_CHECK)
		libmaus2::sorting::SerialisingSortingBufferedOutputFile<ExonInterval> & SSBOF = (*sortPtr)[tid];
		#endif

		for ( uint64_t z = 0; z < tsize; ++z )
		{
			libmaus2::bambam::BamAlignmentDecoder::readAlignmentGz(*pdecoder,algn);
			int const mapq = algn.getMapQ();

			if (
				algn.isMapped() && !algn.isSecondary() && !algn.isSupplementary() &&
				((algn.getFlags() & excludeflags) == 0) &&
				mapq >= mapqmin &&
				mapq <= mapqmax
			)
			{
				int64_t const annoref1 = Vmap[algn.getRefID()];

				if ( annoref1 >= 0 )
				{
					assert ( static_cast<unsigned int>(annoref1) < VRS.size() );
					assert ( gtfdata.Vchr[annoref1] == bamheader.getRefIDName(algn.getRefID()) );

					if ( algn.isPaired() && algn.isRead1() && algn.isMateMapped() )
					{
						int64_t const annoref2 = Vmap[algn.getRefID()];

						libmaus2::math::IntegerInterval<int64_t> const I1 = algn.getReferenceInterval();
						std::pair<uint64_t,uint64_t> P1(I1.from,I1.from+I1.diameter());
						libmaus2::math::IntegerInterval<int64_t> const I2 = algn.getNextReferenceInterval(Aop);
						std::pair<uint64_t,uint64_t> P2(I2.from,I2.from+I2.diameter());

						#if defined(RANGE_QUERY_CHECK)
						uint64_t const from_1 = I1.from;
						uint64_t const to_1 = from_1 + I1.diameter();
						uint64_t const from_2 = I2.from;
						uint64_t const to_2 = from_2 + I2.diameter();
						IntervalList const & IL_1 = *AIL[annoref1];
						std::pair<uint64_t,uint64_t> const IL_1_o = IL_1.query(std::pair<uint64_t,uint64_t>(from_1,to_1),TC.ILQC_1,AP64[annoref1]->begin(),annoref1);
						IntervalList const & IL_2 = *AIL[annoref2];
						std::pair<uint64_t,uint64_t> const IL_2_o = IL_2.query(std::pair<uint64_t,uint64_t>(from_2,to_2),TC.ILQC_2,AP64[annoref2]->begin(),annoref2);
						#endif

						uint64_t const o1 =
							strandspecific
							?
								(
									algn.isReverse()
									?
									VRS_reverse[annoref1]->search(Exon(P1.first,P1.second),query_R1,query_todo)
									:
									VRS_forward[annoref1]->search(Exon(P1.first,P1.second),query_R1,query_todo)
								)
								:
								VRS[annoref1]->search(Exon(P1.first,P1.second),query_R1,query_todo)
							;
						uint64_t const o2 =
							strandspecific
							?
								(
									algn.isReverse()
									?
									VRS_forward[annoref2]->search(Exon(P2.first,P2.second),query_R2,query_todo)
									:
									VRS_reverse[annoref2]->search(Exon(P2.first,P2.second),query_R2,query_todo)
								)
								:
								VRS[annoref2]->search(Exon(P2.first,P2.second),query_R2,query_todo)
							;

						uint64_t AGID_o1 = 0;
						uint64_t ATID_o1 = 0;

						for ( uint64_t i = 0; i < o1; ++i )
						{
							Exon const & exon = *(query_R1[i]);
							assert ( exon == gtfdata.Aexon[exon.id] );
							Transcript const & transcript = gtfdata.Atranscript[exon.transcript_id];
							uint64_t const gene_id = transcript.gene_id;
							AGID1.push(AGID_o1,gene_id);
							ATID1.push(ATID_o1,exon.transcript_id);
						}

						std::sort(AGID1.begin(),AGID1.begin()+AGID_o1);
						AGID_o1 = std::unique(AGID1.begin(),AGID1.begin()+AGID_o1) - AGID1.begin();
						std::sort(ATID1.begin(),ATID1.begin()+ATID_o1);
						ATID_o1 = std::unique(ATID1.begin(),ATID1.begin()+ATID_o1) - ATID1.begin();

						uint64_t AGID_o2 = 0;
						uint64_t ATID_o2 = 0;

						for ( uint64_t i = 0; i < o2; ++i )
						{
							Exon const & exon = *(query_R2[i]);
							assert ( exon == gtfdata.Aexon[exon.id] );
							Transcript const & transcript = gtfdata.Atranscript[exon.transcript_id];
							uint64_t const gene_id = transcript.gene_id;
							AGID2.push(AGID_o2,gene_id);
							ATID2.push(ATID_o2,exon.transcript_id);
						}

						std::sort(AGID2.begin(),AGID2.begin()+AGID_o2);
						AGID_o2 = std::unique(AGID2.begin(),AGID2.begin()+AGID_o2) - AGID2.begin();
						std::sort(ATID2.begin(),ATID2.begin()+ATID_o2);
						ATID_o2 = std::unique(ATID2.begin(),ATID2.begin()+ATID_o2) - ATID2.begin();

						if ( annoref1 == annoref2 )
						{
							int64_t const annoref = annoref1;

							if ( AGID_o1 == 1 && AGID_o2 == 1 && AGID1[0] == AGID2[0] )
							{
								#if defined(RANGE_QUERY_CHECK)
								// merge transcript id list
								uint64_t i_1 = 0;
								uint64_t i_2 = 0;
								uint64_t ATID_o = 0;
								while ( i_1 < ATID_o1 && i_2 < ATID_o2 )
									if (
										ATID1[i_1] < ATID2[i_2]
									)
										++i_1;
									else if (
										ATID2[i_2] < ATID1[i_1]
									)
										++i_2;
									else
									{
										assert ( ATID1[i_1] == ATID2[i_2] );

										uint64_t const transid = ATID1[i_1];

										++i_1;
										++i_2;

										ATID1[ATID_o++] = transid;
									}

								for ( uint64_t i = 1; i < ATID_o; ++i )
									assert ( ATID1[i-1] < ATID1[i] );

								if ( ATID_o > 0 )
								{
									for ( uint64_t i = 0; i < IL_1_o.second; ++i )
									{
										ExonInterval const & EI = TC.ILQC_1.EI[i];

										uint64_t const intid = IL_1.getId(EI);
										IntervalList::IntervalWithId const & IWID = IL_1.V[intid];

										// std::cerr << I1 << " " << EI << " " << IWID.toString() << std::endl;

										assert ( EI.from >= IWID.Q.first );
										assert ( EI.to   <= IWID.Q.second );
										assert ( IWID.end > IWID.start );

										bool found = false;

										for ( uint64_t j = IWID.start; j < IWID.end; ++j )
										{
											Exon const & E = (*(AP64[annoref1]))[IL_1.Aid[j]];

											assert ( static_cast<int64_t>(EI.from) >= E.getFrom() );
											assert ( static_cast<int64_t>(EI.to  ) <= E.getTo()   );

											uint64_t const transid = E.transcript_id;
											uint64_t const * ATID_p = std::lower_bound(ATID1.begin(),ATID1.begin()+ATID_o,transid);
											if ( ATID_p != ATID1.begin()+ATID_o && *ATID_p == transid )
											{
												found = true;
											}
										}

										if ( found )
										{
											SSBOF.put(EI);
										}
									}
									for ( uint64_t i = 0; i < IL_2_o.second; ++i )
									{
										ExonInterval const & EI = TC.ILQC_2.EI[i];

										uint64_t const intid = IL_2.getId(EI);
										IntervalList::IntervalWithId const & IWID = IL_2.V[intid];

										// std::cerr << I2 << " " << EI << " " << IWID.toString() << std::endl;

										assert ( EI.from >= IWID.Q.first );
										assert ( EI.to   <= IWID.Q.second );
										assert ( IWID.end > IWID.start );

										bool found = false;

										for ( uint64_t j = IWID.start; j < IWID.end; ++j )
										{
											Exon const & E = (*(AP64[annoref2]))[IL_2.Aid[j]];

											assert ( static_cast<int64_t>(EI.from) >= E.getFrom() );
											assert ( static_cast<int64_t>(EI.to  ) <= E.getTo()   );

											uint64_t const transid = E.transcript_id;
											uint64_t const * ATID_p = std::lower_bound(ATID2.begin(),ATID2.begin()+ATID_o,transid);
											if ( ATID_p != ATID2.begin()+ATID_o && *ATID_p == transid )
											{
												found = true;
											}
										}

										if ( found )
										{
											SSBOF.put(EI);
										}
									}
								}
								#endif

								geneFreq [AGID1[0]] += 1;
								geneBases[AGID1[0]] += algn.getReferenceLength() + algn.getNextReferenceLength(Aop);

								for ( uint64_t i = 0; i < o1; ++i )
								{
									Exon const & exon = *(query_R1[i]);
									exonFreq[exon.id] += 1;
								}
								for ( uint64_t i = 0; i < o2; ++i )
								{
									Exon const & exon = *(query_R2[i]);
									exonFreq[exon.id] += 1;
								}

								numunique += 1;
								Vnumunique[annoref] += 1;
							}
							else if ( AGID_o1 == 0 || AGID_o2 == 0 )
							{
								numunmatched += 1;
								Vnumunmatched[annoref] += 1;
							}
							else
							{
								numambig += 1;
								Vnumambig[annoref] += 1;
							}
						}
						else
						{
							numsplit += 1;
						}
					}
					else if ( algn.isPaired() && !algn.isMateMapped() )
					{
						numunmatched += 1;
						Vnumunmatched[annoref1] += 1;
					}

					#if 0
					else if ( ! algn.isPaired() || ! algn.isMateMapped() )
					{

						uint64_t const from = IA.from;
						uint64_t const to = from + IA.diameter();

						uint64_t const o =
							strandspecific
							?
								(
								algn.isRead1()
									?
									(
										algn.isReverse()
										?
										VRS_reverse[annoref]->search(Exon(from,to),query_R,query_todo)
										:
										VRS_forward[annoref]->search(Exon(from,to),query_R,query_todo)
									)
									:
									(
										algn.isReverse()
										?
									VRS_forward[annoref]->search(Exon(from,to),query_R,query_todo)
										:
										VRS_reverse[annoref]->search(Exon(from,to),query_R,query_todo)
									)
								)
								:
								VRS[annoref]->search(Exon(from,to),query_R,query_todo);
								;

						#if defined(RANGE_QUERY_CHECK)
						IntervalList const & IL = *AIL[annoref];

						uint64_t const ILo = IL.query(std::pair<uint64_t,uint64_t>(from,to),ILQC,AP64[annoref]->begin());
						std::vector<Exon> VEA(ILQC.E.begin(),ILQC.E.begin()+ILo);
						std::sort(VEA.begin(),VEA.end(),ExonIdOffsetComparator());
						std::vector<Exon> VEB(o);
						for ( uint64_t i = 0; i < o; ++i )
							VEB[i] = *(query_R[i]);
						std::sort(VEB.begin(),VEB.end(),ExonIdOffsetComparator());

						if ( ILo != o )
						{
							std::cerr << "ILo=" << ILo << " o=" << o << std::endl;
							exit(1);
						}
						else
						{
							for ( uint64_t i = 0; i < o; ++i )
								if ( VEA[i].id_offset != VEB[i].id_offset )
								{
									std::cerr << VEA[i].id_offset << " != " <<  VEB[i].id_offset << std::endl;
									exit(1);
								}
						}
						#endif

						#if defined(VERBOSE)
						std::cerr << IA << " " << algn.formatAlignment(bamheader) << std::endl;
						#endif

						uint64_t AGID_o = 0;

						for ( uint64_t i = 0; i < o; ++i )
						{
							Exon const & exon = *(query_R[i]);
							Transcript const & transcript = Atranscript[exon.transcript_id];
							uint64_t const gene_id = transcript.gene_id;
							AGID.push(AGID_o,gene_id);

							#if defined(VERBOSE)
							char const * exid = Aid.begin() + exon.id_offset;
							std::cerr << "\t[" << i <<"]\t" << exon.getFrom() << "\t" << exon.getTo() << "\t" << exid << std::endl;
							#endif
						}


						std::sort(AGID.begin(),AGID.begin()+AGID_o);
						AGID_o = std::unique(AGID.begin(),AGID.begin()+o) - AGID.begin();

						if ( AGID_o == 0 )
						{
							numunmatched += 1;
							Vnumunmatched[annoref] += 1;
						}
						else if ( AGID_o == 1 )
						{
							Agene[
								AGID[0]
							].freq += 1;

							numunique += 1;
							Vnumunique[annoref] += 1;
						}
						else
						{
							#if 0
							for ( uint64_t i = 0; i < AGID_o; ++i )
							{
								std::cerr << "[" << i << "]=" << print(Agene[AGID[i]],Aid) << std::endl;
							}
							#endif

							numambig += 1;
							Vnumambig[annoref] += 1;
						}
					}
					#endif
				}
			}

			#if 0
			if ( ((++algnid) % (1024*1024) == 0) )
			{
				std::cerr << "[V]\tprocessed\t" << algnid << "\t" << bamheader.getRefIDName(algn.getRefID()) << "\t" << algn.getPos() << "\t" << numunmatched << "\t" << numunique << "\t" << numambig << "\t" << numsplit << std::endl;
			}
			#endif
		}

		uint64_t const lpackcomplete = ++packComplete;

		if ( verbose )
		{
			libmaus2::parallel::ScopeStdSpinLock slock(libmaus2::aio::StreamLock::cerrlock);
			std::cerr << "[V] complete\t" << lpackcomplete << "/" << alpacks << "\t" << static_cast<double>(lpackcomplete) / alpacks << std::endl;
		}
	}


	#if defined(RANGE_QUERY_CHECK)
	{

		libmaus2::autoarray::AutoArray<ExonInfo> AEI(gtfdata.Aexon.size());

		libmaus2::sorting::SerialisingSortingBufferedOutputFileArray<ExonInterval>::merger_ptr_type merger(
			sortPtr->getMergerParallel(numthreads)
		);
		libmaus2::aio::GenericPeeker<
			libmaus2::sorting::SerialisingSortingBufferedOutputFileArray<ExonInterval>::merger_type
		> GP(*merger);
		libmaus2::util::FiniteSizeHeap<uint64_t> FSHD(0);

		ExonInterval EI;
		while ( GP.peekNext(EI) )
		{
			uint64_t const chrid = EI.chr;
			uint64_t nextintid = 0;
			IntervalList const & IL = *(AIL[chrid]);

			struct DepthCallback
			{
				uint64_t uncovered;
				uint64_t basecover;
				uint64_t total;

				DepthCallback()
				: uncovered(0), basecover(0), total(0) {}

				void operator()(uint64_t const from, uint64_t const to, uint64_t const depth)
				{
					// std::cerr << "[" << from << "," << to << ") " << depth << "\n";

					uint64_t const l = to-from;

					if ( depth )
					{
						basecover += l * depth;
					}
					else
					{
						uncovered += l;
					}

					total += l;
				}

				void print(std::ostream & out) const
				{
					if ( total )
					{
						double const dtotal = total;
						out << "uncovered " << uncovered/dtotal << " avg " << basecover/dtotal << std::endl;
					}
					else
					{
						out << "uncovered " << 1 << " avg " << 0 << std::endl;
					}
				}
			};

			while ( GP.peekNext(EI) && EI.chr == chrid )
			{
				// interval id
				uint64_t const intid = IL.getId(EI);
				while ( nextintid < intid )
				{
					IntervalList::IntervalWithId const & IWI = IL.get(nextintid++);

					bool const unique = IWI.end-IWI.start == 1;
					for ( uint64_t z = IWI.start; z < IWI.end; ++z )
					{
						uint64_t const index = IL.Aid[z];
						Exon const & PE = (*(AP64[chrid]))[index];
						uint64_t const exonid = PE.id;

						ExonInfo & info = AEI[exonid];
						if ( unique )
						{
							info.unique.uncovered += IWI.Q.second-IWI.Q.first;
							info.unique.total     += IWI.Q.second-IWI.Q.first;
						}
						else
						{
							info.ambig.uncovered += IWI.Q.second-IWI.Q.first;
							info.ambig.total     += IWI.Q.second-IWI.Q.first;

						}
					}
				}
				assert ( nextintid == intid );

				IntervalList::IntervalWithId const & IWI = IL.get(intid);

				// std::cerr << std::string(30,'-') << IL.V[intid].toString() << std::endl;

				DepthCallback DC;
				uint64_t prevstart = IWI.Q.first;
				uint64_t depth = 0;

				while ( GP.peekNext(EI) && EI.chr == chrid && IL.getId(EI) == intid )
				{
					while ( !FSHD.empty() && FSHD.top() <= EI.from )
					{
						uint64_t const e = FSHD.pop();

						if ( e > prevstart )
						{
							// std::cerr << "[" << prevstart << "," << e << ") " << depth << "\n";
							DC(prevstart,e,depth);
							// [prevstart,e)
							prevstart = e;
						}

						depth -= 1;
					}

					if ( EI.from > prevstart )
					{
						// std::cerr << "[" << prevstart << "," << EI.from << ") " << depth << "\n";
						DC(prevstart,EI.from,depth);
						// [prevstart,e)
						prevstart = EI.from;
					}

					depth += 1;
					FSHD.pushBump(EI.to);

					GP.getNext(EI);
				}

				while ( !FSHD.empty() )
				{
					uint64_t const e = FSHD.pop();

					if ( e > prevstart )
					{
						// std::cerr << "[" << prevstart << "," << e << ") " << depth << "\n";
						DC(prevstart,e,depth);
						// [prevstart,e)
						prevstart = e;
					}

					depth -= 1;
				}

				assert ( depth == 0 );

				if ( prevstart < IWI.Q.second )
				{
					DC(prevstart,IWI.Q.second,depth);
					// std::cerr << "[" << prevstart << "," << IWI.Q.second << ") " << depth << "\n";
				}

				// DC.print(std::cerr);

				bool const unique = IWI.end-IWI.start == 1;
				for ( uint64_t z = IWI.start; z < IWI.end; ++z )
				{
					uint64_t const index = IL.Aid[z];
					Exon const & PE = (*(AP64[chrid]))[index];
					uint64_t const exonid = PE.id;

					ExonInfo & info = AEI[exonid];
					if ( unique )
					{
						info.unique.uncovered += DC.uncovered;
						info.unique.basecount += DC.basecover;
						info.unique.total += DC.total;
					}
					else
					{
						info.ambig.uncovered += DC.uncovered;
						info.ambig.basecount += DC.basecover;
						info.ambig.total += DC.total;

					}
				}

				nextintid += 1;
			}

			while ( nextintid < IL.V_o )
			{
				IntervalList::IntervalWithId const & IWI = IL.get(nextintid++);

				bool const unique = IWI.end-IWI.start == 1;
				for ( uint64_t z = IWI.start; z < IWI.end; ++z )
				{
					uint64_t const index = IL.Aid[z];
					Exon const & PE = (*(AP64[chrid]))[index];
					uint64_t const exonid = PE.id;

					ExonInfo & info = AEI[exonid];
					if ( unique )
					{
						info.unique.uncovered += IWI.Q.second-IWI.Q.first;
						info.unique.total     += IWI.Q.second-IWI.Q.first;
					}
					else
					{
						info.ambig.uncovered += IWI.Q.second-IWI.Q.first;
						info.ambig.total     += IWI.Q.second-IWI.Q.first;

					}
				}
			}
			assert ( nextintid == IL.V_o );

		}

		libmaus2::autoarray::AutoArray<ExonInfo> ATI(gtfdata.Atranscript.size());

		for ( uint64_t i = 0; i < AEI.size(); ++i )
		{
			Exon const & E = gtfdata.Aexon[i];
			ExonInfo const & I = AEI[i];
			uint64_t const transcript_id = E.transcript_id;
			ATI[transcript_id] += I;
		}

		libmaus2::autoarray::AutoArray<uint64_t> ATTI;

		for ( uint64_t g = 0; g < gtfdata.Agene.size(); ++g )
		{
			Gene const & G = gtfdata.Agene[g];
			char const * name = gtfdata.Aid.begin() + G.name_offset;
			uint64_t o = 0;

			std::string const refidname = bamheader.getRefIDName(G.chr_id);


			for ( uint64_t ti = G.start; ti < G.end; ++ti )
				if (
					ATI[ti].getUncoveredFraction() <= uncoveredthres
					&&
					ATI[ti].unique.getUncoveredFraction() <= uniqueuncoveredthres
				)
					ATTI.push(o,ti);

			struct UncoveredComp
			{
				libmaus2::autoarray::AutoArray<ExonInfo> & ATI;
				UncoveredComp(libmaus2::autoarray::AutoArray<ExonInfo> & rATI)
				: ATI(rATI)
				{
				}

				bool operator()(uint64_t const i, uint64_t const j) const
				{
					return ATI[i].getUncoveredFraction() > ATI[j].getUncoveredFraction();
				}
			};

			if ( o )
			{
				std::sort(ATTI.begin(),ATTI.begin()+o,UncoveredComp(ATI));

				for ( uint64_t iti = 0; iti < o; ++iti )
				{
					uint64_t const ti = ATTI[iti];
					Transcript const & T = gtfdata.Atranscript[ti];
					char const * transid = gtfdata.Aid.begin() + T.id_offset;

					std::cout << "[transcript]\t" << refidname << ":" << name
						<< "\t" << transid << "\t"
						<< ATI[ti].unique
						<< "\t"
						<< ATI[ti].ambig
						<< "\t"
						<< ATI[ti];

					for ( uint64_t ei = T.start; ei < T.end; ++ei )
					{
						Exon const & E = gtfdata.Aexon[ei];
						std::cout << "\t";
						E.printCoord(std::cout);
					}

					std::cout << "\n";
				}
			}
		}
	}
	#endif

	if ( verbose )
		std::cerr << "[V]\tprocessed\t" << algnid << "\t" << bamheader.getRefIDName(-1) << "\t" << -1 << "\t" << numunmatched << "\t" << numunique << "\t" << numambig << "\t" << numsplit << std::endl;

	if ( verbose )
		for ( uint64_t i = 0; i < gtfdata.Vchr.size(); ++i )
			std::cerr << "[C]\t" << gtfdata.Vchr[i] << "\t" << Vnumunmatched[i] << "\t" << Vnumunique[i] << "\t" << Vnumambig[i] << "\n";

	uint64_t sumfreq = 0;
	for ( uint64_t i = 0; i < gtfdata.next_gene_id; ++i )
		sumfreq += geneFreq[i];
	double const dsumfreq = sumfreq;

	for ( uint64_t i = 0; i < gtfdata.next_gene_id; ++i )
	{
		Gene const & gene = gtfdata.Agene[i];
		char const * gene_id = gtfdata.Aid.begin() + gene.id_offset;
		uint64_t const freq = geneFreq[i];
		double const tpm = 1e6 * (sumfreq ? freq/dsumfreq : 0);

		std::cout << "[gene]\t" << gene_id << "\t" << freq << "\t" << tpm << std::endl;
	}

	return EXIT_SUCCESS;
}

#include <libmaus2/fastx/FastaPeeker.hpp>
#include <libmaus2/lcs/SuffixArrayLCS.hpp>

int main(int argc, char * argv[])
{
	try
	{
		::libmaus2::util::ArgInfo const arginfo(argc,argv);

		for ( uint64_t i = 0; i < arginfo.restargs.size(); ++i )
			if (
				arginfo.restargs[i] == "-v"
				||
				arginfo.restargs[i] == "--version"
			)
			{
				std::cerr << ::biobambam2::Licensing::license();
				return EXIT_SUCCESS;
			}
			else if (
				arginfo.restargs[i] == "-h"
				||
				arginfo.restargs[i] == "--help"
			)
			{
				std::cerr << printHelpMessage(arginfo);
				return EXIT_SUCCESS;
			}

		if ( arginfo.getNumRestArgs() < 2 )
		{
			std::cerr << printHelpMessage(arginfo);
			return EXIT_FAILURE;
		}

		if ( arginfo.getValue<int>("comparefasta",0) )
		{
			std::string const fa0 = arginfo.getUnparsedRestArg(0);
			std::string const fa1 = arginfo.getUnparsedRestArg(1);

			libmaus2::fastx::FastaPeeker P0(fa0);
			libmaus2::fastx::FastaPeeker P1(fa1);

			libmaus2::fastx::FastAReader::pattern_type pat0;
			libmaus2::fastx::FastAReader::pattern_type pat1;

			while ( P0.peekNext(pat0) && P1.peekNext(pat1) )
			{
				std::string const short0 = pat0.getShortStringId();
				std::string const short1 = pat1.getShortStringId();

				int const r = libmaus2::bambam::StrCmpNum::strcmpnum(short0.c_str(),short1.c_str());

				if ( r < 0 )
				{
					std::cerr << "[V] only in first " << short0 << std::endl;

					P0.getNext(pat0);
				}
				else if ( r > 0 )
				{
					std::cerr << "[V] only in second " << short1 << std::endl;

					P1.getNext(pat1);
				}
				else
				{
					std::cerr << "[V] common " << short0 << std::endl;

					if ( pat0.spattern != pat1.spattern )
					{
						std::cerr << pat0.spattern << std::endl;
						std::cerr << pat1.spattern << std::endl;

						libmaus2::lcs::SuffixArrayLCS::LCSResult res = libmaus2::lcs::SuffixArrayLCS::lcsmin(pat0.spattern,pat1.spattern);

						std::cerr << res.maxlcp << " " << res.maxpos_a << " " << res.maxpos_b << std::endl;

						return EXIT_FAILURE;
					}

					P0.getNext(pat0);
					P1.getNext(pat1);
				}
			}

			return EXIT_SUCCESS;
		}

		return bamfeaturecount(arginfo);
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
