/**
    bambam
    Copyright (C) 2019 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
#include <config.h>

#include <libmaus2/vcf/VCFParser.hpp>
#include <libmaus2/util/ArgInfo.hpp>
#include <libmaus2/lz/BgzfDeflate.hpp>

#include <biobambam2/Licensing.hpp>

#include <libmaus2/util/AutoArrayCharBaseAllocator.hpp>
#include <libmaus2/util/AutoArrayCharBaseTypeInfo.hpp>
#include <libmaus2/util/GrowingFreeList.hpp>
#include <libmaus2/sorting/SortingBufferedOutputFile.hpp>

// static int getDefaultVerbose() { return 0; }
static int getDefaultGZ() { return 0; }
static int getDefaultDedup() { return 0; }

libmaus2::util::AutoArrayCharBaseAllocator AFLA(0);
libmaus2::util::GrowingFreeList< libmaus2::autoarray::AutoArray<char>, libmaus2::util::AutoArrayCharBaseAllocator, libmaus2::util::AutoArrayCharBaseTypeInfo > AFL(AFLA);

struct VCFEntry
{
	public:
	libmaus2::autoarray::AutoArray<char>::shared_ptr_type A;
	uint64_t n;

	uint64_t s_a;
	uint64_t s_b;

	uint64_t c_3_a;
	uint64_t c_3_e;
	uint64_t c_4_a;
	uint64_t c_4_e;

	private:

	public:
	VCFEntry() : A(AFL.get()), n(0), s_a(0), s_b(0), c_3_a(0), c_3_e(0), c_4_a(0), c_4_e(0) {}
	~VCFEntry()
	{
		AFL.put(A);
	}
	VCFEntry(VCFEntry const & O) : A(AFL.get()), n(0), s_a(0), s_b(0), c_3_a(0), c_3_e(0), c_4_a(0), c_4_e(0)
	{
		*this = O;
	}

	void swap(VCFEntry & O)
	{
		std::swap(A    ,O.A);
		std::swap(n    ,O.n);
		std::swap(s_a  ,O.s_a);
		std::swap(s_b  ,O.s_b);
		std::swap(c_3_a,O.c_3_a);
		std::swap(c_3_e,O.c_3_e);
		std::swap(c_4_a,O.c_4_a);
		std::swap(c_4_e,O.c_4_e);
	}

	std::ostream & print(std::ostream & out) const
	{
		out.write(A->begin(),n);
		out.put('\n');
		return out;
	}

	void set(char const * a, char const * e, uint64_t const r_s_a, uint64_t const r_s_b,
		uint64_t r_c_3_a,
		uint64_t r_c_3_e,
		uint64_t r_c_4_a,
		uint64_t r_c_4_e
	)
	{
		n = e-a;
		A->ensureSize(e-a);
		std::copy(a,e,A->begin());
		s_a = r_s_a;
		s_b = r_s_b;
		c_3_a = r_c_3_a;
		c_3_e = r_c_3_e;
		c_4_a = r_c_4_a;
		c_4_e = r_c_4_e;
	}

	VCFEntry & operator=(VCFEntry const & O)
	{
		if ( this != &O )
		{
			A->ensureSize(O.A->size());
			std::copy(
				O.A->begin(),
				O.A->begin() + O.n,
				A->begin()
			);

			n = O.n;
			s_a = O.s_a;
			s_b = O.s_b;
			c_3_a = O.c_3_a;
			c_3_e = O.c_3_e;
			c_4_a = O.c_4_a;
			c_4_e = O.c_4_e;
		}
		return *this;
	}

	std::ostream & serialise(std::ostream & out) const
	{
		libmaus2::util::NumberSerialisation::serialiseNumber(out,n);
		out.write(A->begin(),n);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,s_a);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,s_b);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,c_3_a);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,c_3_e);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,c_4_a);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,c_4_e);
		return out;
	}

	std::istream & deserialise(std::istream & in)
	{
		n = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		A->ensureSize(n);
		in.read(A->begin(),n);
		if ( !in || in.gcount() != static_cast<std::streamsize>(n) )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] VCFEntry::deserialise: failed to read data" << std::endl;
			lme.finish();
			throw lme;
		}
		s_a = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		s_b = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		c_3_a = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		c_3_e = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		c_4_a = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		c_4_e = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		return in;
	}

	bool operator<(VCFEntry const & O) const
	{
		if ( s_a != O.s_a )
		{
			return s_a < O.s_a;
		}
		else if ( s_b != O.s_b )
		{
			return s_b < O.s_b;
		}
		else
		{
			uint64_t const l_3 = c_3_e-c_3_a;
			uint64_t const O_l_3 = O.c_3_e-O.c_3_a;

			int const c3 =
				strncmp(
					A->begin()+c_3_a,
					O.A->begin()+O.c_3_a,
					std::min(l_3,O_l_3)
				);

			if ( c3 != 0 )
				return c3 < 0;
			else if ( l_3 != O_l_3 )
				return l_3 < O_l_3;

			uint64_t const l_4 = c_4_e-c_4_a;
			uint64_t const O_l_4 = O.c_4_e-O.c_4_a;

			int const c4 =
				strncmp(
					A->begin()+c_4_a,
					O.A->begin()+O.c_4_a,
					std::min(l_4,O_l_4)
				);

			if ( c4 != 0 )
				return c4 < 0;
			else if ( l_4 != O_l_4 )
				return l_4 < O_l_4;

			return false;
		}
	}
};

static int64_t parseUInt(char const * a, char const * e)
{
	uint64_t v = 0;

	while ( a != e )
	{
		char const c = *(a++);

		if ( c >= '0' && c <= '9' )
		{
			v *= 10;
			v += c-'0';
		}
		else
		{
			return -1;
		}
	}

	return v;
}

int vcfsort(libmaus2::util::ArgInfo const & arginfo, std::istream & in, std::ostream & out)
{
	bool const gz = arginfo.getValue<int>("gz",getDefaultGZ());
	int const dedup = arginfo.getValue<int>("dedup",getDefaultDedup());
	std::string const tmpfilename = arginfo.getUnparsedValue("T",arginfo.getDefaultTmpFileName());
	libmaus2::util::TempFileRemovalContainer::addTempFile(tmpfilename);

	libmaus2::autoarray::AutoArray< char > contig_A;
	libmaus2::autoarray::AutoArray< std::pair<uint64_t,uint64_t> > contig_O;

	libmaus2::vcf::VCFParser vcf(in);

	// uint64_t n_contig = vcf.getContigNames(contig_A,contig_O);

	std::vector<std::string> const Vcontig = vcf.getContigNames();

	#if 0
	for ( uint64_t i = 0; i < n_contig; ++i )
	{
		std::string const name(contig_A.begin() + contig_O[i].first,contig_A.begin() + contig_O[i].second);
		Vcontig.push_back(name);
	}
	#endif

	::libmaus2::trie::Trie<char> trienofailure;
	trienofailure.insertContainer(Vcontig);
	::libmaus2::trie::LinearHashTrie<char,uint32_t>::unique_ptr_type LHTnofailure(trienofailure.toLinearHashTrie<uint32_t>());

	libmaus2::util::TabEntry<> T;
	VCFEntry VE;

	std::pair<bool, char const *> P;

	libmaus2::sorting::SerialisingSortingBufferedOutputFile<VCFEntry>::unique_ptr_type Psorter(
		new libmaus2::sorting::SerialisingSortingBufferedOutputFile<VCFEntry>(tmpfilename,64*1024)
	);

	while ( (P=vcf.readEntry(T)).first )
	{
		uint64_t const s = T.size();

		if ( s >= 5 )
		{
			char const * a = P.second;
			char const * e = T.get(s-1,a).second;

			std::pair<char const *,char const *> P0(T.get(0,a));
			std::pair<char const *,char const *> P1(T.get(1,a));
			std::pair<char const *,char const *> P3(T.get(3,a));
			std::pair<char const *,char const *> P4(T.get(4,a));

			int64_t const contid = LHTnofailure->searchCompleteNoFailure(P0.first,P0.second);
			int64_t const pos = parseUInt(P1.first,P1.second);

			if ( contid >= 0 && pos >= 0 )
			{
				VE.set(
					a,e,contid,pos,
					P3.first-a,
					P3.second-a,
					P4.first-a,
					P4.second-a
				);
				Psorter->put(VE);
			}
			else
			{
				libmaus2::exception::LibMausException lme;
				lme.getStream() << "[E] unable to parse " << std::string(a,e) << std::endl;
				lme.finish();
				throw lme;
			}
		}
	}

	libmaus2::lz::BgzfOutputStream::unique_ptr_type pBOS;

	if ( gz )
	{
		libmaus2::lz::BgzfOutputStream::unique_ptr_type tBOS(new libmaus2::lz::BgzfOutputStream(out));
		pBOS = UNIQUE_PTR_MOVE(tBOS);
	}

	std::ostream & vout = gz ? *pBOS : out;

	VCFEntry VEprev;
	bool VEprevvalid = false;

	vcf.printText(vout);
	libmaus2::sorting::SerialisingSortingBufferedOutputFile<VCFEntry>::merger_ptr_type Pmerger(Psorter->getMerger());
	while ( Pmerger->getNext(VE) )
	{
		if (
			(!dedup)
			||
			(!VEprevvalid)
			||
			(VEprev < VE)
		)
		{
			VE.print(vout);

			#if 0
			if ( VEprevvalid && VE.s_a == VEprev.s_a && VE.s_b == VEprev.s_b )
			{
				std::cerr << std::string(80,'=') << std::endl;
				VEprev.print(std::cerr);
				VE.print(std::cerr);
			}
			#endif
		}

		VEprev.swap(VE);
		VEprevvalid = true;
	}

	Pmerger.reset();
	Psorter.reset();

	if ( gz )
	{
		pBOS->flush();
		pBOS->addEOFBlock();
		pBOS.reset();
	}

	libmaus2::aio::FileRemoval::removeFile(tmpfilename);

	return EXIT_SUCCESS;
}

int main(int argc, char * argv[])
{
	try
	{
		::libmaus2::util::ArgInfo const arginfo(argc,argv);

		for ( uint64_t i = 0; i < arginfo.restargs.size(); ++i )
			if (
				arginfo.restargs[i] == "-v"
				||
				arginfo.restargs[i] == "--version"
			)
			{
				std::cerr << ::biobambam2::Licensing::license();
				return EXIT_SUCCESS;
			}
			else if (
				arginfo.restargs[i] == "-h"
				||
				arginfo.restargs[i] == "--help"
			)
			{
				std::cerr << ::biobambam2::Licensing::license();
				std::cerr << std::endl;
				std::cerr << "Key=Value pairs:" << std::endl;
				std::cerr << std::endl;

				std::vector< std::pair<std::string,std::string> > V;

				V.push_back ( std::pair<std::string,std::string> ( "gz=<["+::biobambam2::Licensing::formatNumber(getDefaultGZ())+"]>", "compress output (default: 0)" ) );
				V.push_back ( std::pair<std::string,std::string> ( "dedup=<["+::biobambam2::Licensing::formatNumber(getDefaultDedup())+"]>", "remove duplicates by checking columns 1,2,4 and 5 for identity (default: 0)" ) );
				V.push_back ( std::pair<std::string,std::string> ( "T=<["+arginfo.getDefaultTmpFileName()+"]>", "temporary file name used (default: use temp file in current directory)" ) );

				::biobambam2::Licensing::printMap(std::cerr,V);

				std::cerr << std::endl;
				return EXIT_SUCCESS;
			}

		return vcfsort(arginfo,std::cin,std::cout);
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
