/**
    bambam
    Copyright (C) 2019 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
#include <config.h>

#include <libmaus2/vcf/VCFParser.hpp>
#include <libmaus2/util/ArgInfo.hpp>
#include <libmaus2/lz/BgzfDeflate.hpp>

#include <biobambam2/Licensing.hpp>

static int getDefaultVerbose() { return 0; }

int vcfconcat(libmaus2::util::ArgInfo const & arginfo, std::istream & in, std::ostream & out)
{
	//unsigned int const verbose = arginfo.getValue<unsigned int>("verbose",getDefaultVerbose());
	bool const gz = arginfo.getValue<int>("gz",0);

	std::string line;
	std::vector<std::string> Vin;

	while ( std::getline(in,line) )
		Vin.push_back(line);

	libmaus2::lz::BgzfOutputStream::unique_ptr_type pBOS;

	if ( gz )
	{
		libmaus2::lz::BgzfOutputStream::unique_ptr_type tBOS(new libmaus2::lz::BgzfOutputStream(out));
		pBOS = UNIQUE_PTR_MOVE(tBOS);
	}

	std::ostream & vout = gz ? *pBOS : out;

	if ( Vin.size() )
	{
		std::pair<bool, char const *> E;

		{
			libmaus2::aio::InputStreamInstance ISI(Vin[0]);
			libmaus2::vcf::VCFParser vcf(ISI);
			vcf.printText(vout);
			vcf.unparsedCopy(vout);
		}

		for ( uint64_t j = 1; j < Vin.size(); ++j )
		{
			libmaus2::aio::InputStreamInstance ISI(Vin[j]);
			libmaus2::vcf::VCFParser vcf(ISI);
			vcf.unparsedCopy(vout);
		}
	}

	if ( gz )
	{
		pBOS->flush();
		pBOS->addEOFBlock();
		pBOS.reset();
	}

	return EXIT_SUCCESS;
}

int main(int argc, char * argv[])
{
	try
	{
		::libmaus2::util::ArgInfo const arginfo(argc,argv);

		for ( uint64_t i = 0; i < arginfo.restargs.size(); ++i )
			if (
				arginfo.restargs[i] == "-v"
				||
				arginfo.restargs[i] == "--version"
			)
			{
				std::cerr << ::biobambam2::Licensing::license();
				return EXIT_SUCCESS;
			}
			else if (
				arginfo.restargs[i] == "-h"
				||
				arginfo.restargs[i] == "--help"
			)
			{
				std::cerr << ::biobambam2::Licensing::license();
				std::cerr << std::endl;
				std::cerr << "Key=Value pairs:" << std::endl;
				std::cerr << std::endl;

				std::vector< std::pair<std::string,std::string> > V;

				V.push_back ( std::pair<std::string,std::string> ( "verbose=<["+::biobambam2::Licensing::formatNumber(getDefaultVerbose())+"]>", "print progress report (default: 1)" ) );

				::biobambam2::Licensing::printMap(std::cerr,V);

				std::cerr << std::endl;
				return EXIT_SUCCESS;
			}

		return vcfconcat(arginfo,std::cin,std::cout);
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
